module.exports = {
  localeDetection: false,
  locales: ["mk", "en", "sq"],
  defaultLocale: "mk",

  pages: {
    "*": ["common"],
  },
};
