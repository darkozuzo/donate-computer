/** @type {import('next').NextConfig} */
const nextTranslate = require("next-translate-plugin");
module.exports = nextTranslate();
const nextConfig = {
  reactStrictMode: true,
  compiler: {
    styledComponents: true,
  },
  ...nextTranslate(),
};

module.exports = nextConfig;
