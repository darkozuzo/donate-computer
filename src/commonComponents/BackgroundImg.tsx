import { url } from "inspector";
import { NextPage } from "next";
import React from "react";
import styled from "styled-components";
interface Props {
  image: string;
}

const BackgroundImg: NextPage<Props> = ({ image }) => {
  return <BackgImg style={{ backgroundImage: `url(${image} )` }}></BackgImg>;
};

export default BackgroundImg;

const BackgImg = styled.div`
  /* background-image: url("../images/background-Img-1.png"); */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  width: 100%;
  height: 350px;
`;
