import React, { ButtonHTMLAttributes } from "react";
import styled from "styled-components";
interface ButtonProps {
  variant?: "variantOne" | "variantTwo" | "variantThree" | "variantFour";
}
const Button = (
  props: ButtonHTMLAttributes<HTMLButtonElement> & ButtonProps
) => {
  return (
    <Buttons variant={props.variant} {...props}>
      {props.children}
    </Buttons>
  );
};

export default Button;

const Buttons = styled.button<{
  variant?: "variantOne" | "variantTwo" | "variantThree" | "variantFour";
}>`
  padding: 10px 50px;
  border-radius: 13px;
  font-weight: 600;
  background-color: ${(props) =>
    props.variant === "variantOne"
      ? props.theme.colors.customRed
      : props.variant === "variantTwo"
      ? props.theme.colors.custom4
      : props.theme.colors.white};
  color: ${(props) =>
    props.variant === "variantOne"
      ? props.theme.colors.white
      : props.variant === "variantTwo"
      ? props.theme.colors.white
      : props.variant === "variantThree"
      ? props.theme.colors.black
      : props.theme.colors.customRed};
  border: 2px solid
    ${(props) =>
      props.variant === "variantOne"
        ? props.theme.colors.customRed
        : props.variant === "variantTwo"
        ? props.theme.colors.custom4
        : props.theme.colors.customRed}!important;

  &:hover {
    background-color: ${(props) =>
      props.variant === "variantOne"
        ? props.theme.colors.customDarkRed
        : "none"};

    color: ${(props) =>
      props.variant === "variantThree" ? props.theme.colors.customRed : "none"};
    text-decoration: ${(props) =>
      props.variant === "variantFour" ? "underline" : "none"};

    border: 2px solid
      ${(props) =>
        props.variant === "variantOne"
          ? props.theme.colors.customDarkRed
          : "none"}!important;
  }
`;
