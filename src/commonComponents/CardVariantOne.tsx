// import { NextPage } from "next";
// import Link from "next/link";
// import React from "react";
// import Card from "react-bootstrap/Card";
// interface Props {
//   id: number;
//   img: string;
//   title: string;
//   preTitle: string;
//   text: string;
//   date: string;
// }

// const CardVariantOne: NextPage<Props> = ({
//   id,
//   img,
//   title,
//   preTitle,
//   text,
//   date,
// }) => {
//   return (
//     <Card style={{ width: "18rem" }} className="text-start">
//       <Card.Img
//         variant="top"
//         src={img}
//         style={{ width: "100%", height: "200px" }}
//       />
//       <span
//         style={{
//           background: "#D73F3B",
//           width: "25%",
//           textAlign: "center",
//           marginTop: "-13px",
//           color: "white",
//         }}
//       >
//         {preTitle}
//       </span>
//       <Card.Body>
//         <Card.Title>{title}</Card.Title>
//         <Card.Text>{text}</Card.Text>
//         <div className="d-flex justify-content-between">
//           <p>{date}</p>
//           <Link href="/blog">Повеќе</Link>
//         </div>
//       </Card.Body>
//     </Card>
//   );
// };

// export default CardVariantOne;

/*^gorniot nacin e tip na karticka koj e zavisen od kontent*/
/*dolniot nacin na karticka e nacin na koj goleminata na kontent ne vlijae postavena mu e fiksna golemina*/

import { NextPage } from "next";
import Link from "next/link";
import React from "react";
import Card from "react-bootstrap/Card";

interface Props {
  id: number;
  img: string;
  title: string;
  preTitle: string;
  text: string;
  date: string;
}

const CardVariantOne: NextPage<Props> = ({
  id,
  img,
  title,
  preTitle,
  text,
  date,
}) => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
      }}
    >
      <Card
        style={{
          width: "18rem",
          height: "400px",
          margin: "10px",
          display: "flex",
          flexDirection: "column",
        }}
        className="text-start"
      >
        <Card.Img
          variant="top"
          src={img}
          style={{ width: "100%", height: "200px" }}
        />
        <span
          style={{
            background: "#D73F3B",
            width: "25%",
            textAlign: "center",
            marginTop: "-13px",
            color: "white",
          }}
        >
          {preTitle}
        </span>
        <Card.Body
          style={{
            flexGrow: 1,
          }}
        >
          <Card.Title>{title}</Card.Title>
          <Card.Text style={{ overflowY: "hidden", height: "100px" }}>
            {text}
          </Card.Text>
        </Card.Body>
        <div
          style={{ padding: "0 16px" }}
          className="d-flex justify-content-between"
        >
          <p>{date}</p>
          <Link href={`/blog/${id}`}>Повеќе</Link>
        </div>
      </Card>
    </div>
  );
};

export default CardVariantOne;
