import { NextPage } from "next";
import React from "react";
import Card from "react-bootstrap/Card";
interface Props {
  title: string;
  preTitle: string;
  img: string;
  text: string;
}

const CardVariantTwo: NextPage<Props> = ({ img, preTitle, title, text }) => {
  return (
    <Card style={{ width: "18rem" }} className="text-start">
      <Card.Img
        variant="top"
        src={img}
        style={{ width: "100%", height: "200px", objectFit: "cover" }}
      />
      <span
        style={{
          background: "#D73F3B",
          textTransform: "uppercase",
          fontSize: "14px",
          textAlign: "center",
          color: "white",
        }}
      >
        {preTitle}
      </span>
      <Card.Body className="text-center">
        <Card.Title>
          <b>{title}</b>
        </Card.Title>
        <Card.Text>{text}</Card.Text>
      </Card.Body>
    </Card>
  );
};

export default CardVariantTwo;
