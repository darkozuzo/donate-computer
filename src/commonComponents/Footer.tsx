import React, { useState } from "react";
import styled from "styled-components";
import Button from "./Button";
import { NextPage } from "next";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";

const FooterPage: NextPage = () => {
  const [name, setName] = useState<string>("");
  const [email, setEmail] = useState<string | number>("");
  const { t } = useTranslation("common");

  return (
    <Footer>
      <div className="container">
        <div className="row">
          <div className="col-4">
            <Logo src="../images/logo.png" alt="" />
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Repudiandae itaque odio est dolor sint.
            </p>
            <form
              onSubmit={(event: React.FormEvent<HTMLFormElement>) => {
                event.preventDefault();

                /*@ts-ignore */
                console.table(name, email);
                setName("");
                setEmail("");
              }}
            >
              <div className="form-group mb-2">
                <input
                  type="text"
                  className="form-control"
                  id="formGroupExampleInput"
                  placeholder={t("footer.inputName")}
                  value={name}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    setName(event.target.value);
                  }}
                />
              </div>
              <div className="form-group mb-4">
                <input
                  type="text"
                  className="form-control"
                  id="formGroupExampleInput2"
                  placeholder={t("footer.inputEmail")}
                  value={email}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    setEmail(event.target.value);
                  }}
                />
              </div>
              <Button variant="variantTwo" type="submit">
                {t("footer.footerButton")}
              </Button>
            </form>
          </div>
          <div className="col-4 text-center d-flex align-items-center flex-column justify-content-between">
            <div>
              <Title>{t("footer.title")}</Title>
              <a href="mailto:donirajkompjuter@gmail.com">
                donirajkompjuter@gmail.com
              </a>
              <br></br>
              <a href="tel://+38971228802">+38971228802</a>
              <p className="mb-0">ул. Браќа Хаџи Тефови</p>
              <p>бр.38 Кавадарци</p>
            </div>
            <Gifts>
              <Title>{t("footer.title2")}</Title>
              <div className="d-flex justify-content-between align-items-center">
                <Link href="http://twitter.com">
                  <img src="/images/gifts/twitter-gift.gif" alt="" />
                </Link>
                <Link href="http://linkedIn.com">
                  <img src="/images/gifts/linkedIn-gift.gif" alt="" />
                </Link>
                <Link href="http://instagram.com">
                  <img src="/images/gifts/instagram-gift.gif" alt="" />
                </Link>
                <Link href="http://facebook.com">
                  <img src="/images/gifts/facebook-gift.gif" alt="" />
                </Link>
              </div>
              <div className="footerVector">
                <img src="vectors/footerVector-lines.svg" alt="" />
              </div>
            </Gifts>
          </div>
          <div className="col-4">
            <div className="vector-footer-img d-flex justify-content-center align-items-start flex-column">
              <Title>{t("footer.title3")}</Title>
              <LinkBlack href="/aboutUs">{t("footer.link1")}</LinkBlack>
              <span>{t("footer.link2")}</span>
              <LinkBlack href="/blog">{t("footer.link3")}</LinkBlack>
              <LinkBlack href="/contactUs">{t("footer.link4")}</LinkBlack>
              <br />
              <LinkRed href="/donate">{t("footer.link5")}</LinkRed>
              <LinkRed href="/applyForPc-Eq">{t("footer.link6")}</LinkRed>
            </div>
          </div>
        </div>
      </div>
    </Footer>
  );
};

export default FooterPage;

const Footer = styled.div`
  border-top: 10px dotted ${({ theme }) => theme.colors.customRed};

  padding: ${({ theme }) => theme.spacers.XXL}px 0;

  .vector-footer-img {
    background-image: url("./images/Vector.svg");
    background-position: center;
    background-repeat: no-repeat;
    background-size: contain;
    width: 100%;
    height: 360px;
    padding-left: 185px;
  }
  .col-4 {
    padding: 0px;
  }
`;
const Logo = styled.img`
  width: 130px;
`;
const Title = styled.div`
  font-size: ${({ theme }) => theme.fonts.font_25}px;
  font-weight: 600;
  margin-bottom: ${({ theme }) => theme.spacers.XL}px;
`;
const Gifts = styled.div`
  padding: 0 60px 40px 60px;
  width: 100%;
  position: relative;
  img {
    width: 50px;
  }
  .footerVector {
    position: absolute;
    top: 70px;
    left: 140px;
    z-index: -1;
  }
  .footerVector img {
    width: 110px;
  }
`;
const LinkBlack = styled(Link)`
  color: black;
  text-decoration: none;
  &:hover {
    color: ${({ theme }) => theme.colors.customRed};
  }
`;
const LinkRed = styled(Link)`
  color: ${({ theme }) => theme.colors.customRed};
  text-decoration: none;
  &:hover {
    color: black;
  }
`;
