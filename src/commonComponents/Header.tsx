import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import styled from "styled-components";
import Button from "./Button";
import { NextPage } from "next";
import useTranslation from "next-translate/useTranslation";
import Marquee from "react-fast-marquee";
interface Props {
  isShowModal: boolean;
  setIsShowModal: (value: boolean) => void;
}

const Header: NextPage<Props> = ({ isShowModal, setIsShowModal }) => {
  const router = useRouter();
  const [isQuestion, setIsQuestion] = useState(false);
  const { t } = useTranslation("common");
  return (
    <>
      {router.pathname === "/" && (
        <div className="container p-0">
          <Logos pauseOnHover={true} speed={150}>
            <Link href={"https://klimi.mk/"} target="_blank">
              <img src="/images/logo-images/klimi.png" alt="" />
            </Link>
            <Link href={"https://www.triglav.mk/"} target="_blank">
              <img src="/images/logo-images/triglav.png" alt="" />
            </Link>
            <Link
              href={
                "https://symphony.is/blog/new-symphony-office-were-headed-skopje"
              }
              target="_blank"
            >
              <img src="/images/logo-images/symphony.png" alt="" />
            </Link>
            <Link href={"https://www.allocatesoftware.com"} target="_blank">
              <img src="/images/logo-images/allocate.png" alt="" />
            </Link>
            <Link href={"https://www.inside.com.mk/"} target="_blank">
              <img src="/images/logo-images/inside.png" alt="" />
            </Link>

            <Link
              href={
                "https://symphony.is/blog/new-symphony-office-were-headed-skopje"
              }
              target="_blank"
            >
              <img src="/images/logo-images/symphony.png" alt="" />
            </Link>
            <Link href={"https://www.allocatesoftware.com"} target="_blank">
              <img src="/images/logo-images/allocate.png" alt="" />
            </Link>
          </Logos>
        </div>
      )}
      <Navbar
        style={{
          position: `${router.pathname === "/" ? "static" : "fixed"}`,
          top: `${router.pathname === "/" ? "120px" : "0px"}`,
          left: "0%",
          zIndex: "99",
          width: "100%",
        }}
      >
        <div className="container">
          <div className="row justify-content-between align-items-center">
            <div className="col-2 logo">
              <Link href="/">
                <img className="logo" src="../images/logo.png" alt="" />
              </Link>
            </div>
            <div className="col-8 offset-2">
              <ul className="d-flex justify-content-between align-items-center">
                <li
                  className={`${
                    router.pathname === "/aboutUs" ? "active" : ""
                  }`}
                >
                  <Link href="/aboutUs">{t("nav.aboutUs")}</Link>
                </li>
                <li
                  className={`${
                    router.pathname === "/marketing" ||
                    router.pathname === "/education" ||
                    router.pathname === "/volunteering" ||
                    router.pathname === "/contributeDiff"
                      ? "active"
                      : ""
                  }`}
                >
                  {t("nav.services")} &#x25BE;
                  <ul className="dropdown">
                    <li>
                      <Link href="/marketing">{t("nav.marketing")}</Link>
                    </li>
                    <li>
                      <Link href="/education">{t("nav.education")}</Link>
                    </li>
                    <li>
                      <Link href="/volunteering">{t("nav.volunteering")}</Link>
                    </li>
                    <li>
                      <Link href="/contributeDiff">
                        {t("nav.contributeDiff")}
                      </Link>
                    </li>
                  </ul>
                </li>
                <li
                  className={`${
                    router.pathname === "/blog" ||
                    router.pathname === "/blog/[id]"
                      ? "active"
                      : ""
                  }`}
                >
                  <Link href="/blog">{t("nav.blog")}</Link>
                </li>
                <li
                  className={`${
                    router.pathname === "/contactUs" ? "active" : ""
                  }`}
                >
                  <Link href="/contactUs">{t("nav.contactUs")}</Link>
                </li>
                <li>
                  <Button variant="variantThree">
                    <Link href="/cta">{t("nav.cta")}</Link>
                  </Button>
                </li>
                <li style={{ cursor: "pointer" }}>
                  <div className="d-flex align-items-end">
                    <img
                      className="globe"
                      src="/images/gifts/globe.gif"
                      alt=""
                    />
                    <span style={{ width: "22px" }}>
                      <b>
                        {router.locale === "mk"
                          ? "MK"
                          : router.locale === "en"
                          ? "EN"
                          : "SQ"}
                      </b>
                    </span>
                  </div>
                  <ul className="dropdown">
                    <Link locale="mk" href={router.pathname}>
                      <li>Македонски</li>
                    </Link>
                    <Link locale="en" href={router.pathname}>
                      <li>English</li>
                    </Link>
                    <Link
                      locale="sq"
                      href={
                        router.pathname === `/blog/${router.query}`
                          ? router.asPath
                          : router.pathname
                      }
                    >
                      <li>Squip</li>
                    </Link>
                  </ul>
                </li>
                <li
                  style={{
                    cursor: "pointer",
                  }}
                >
                  <div
                    className="faq-button"
                    onClick={() => {
                      setIsShowModal(true);
                    }}
                  >
                    <img src="/images/icon-faq.png" alt="" />
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </Navbar>
      {isShowModal && (
        <Faq
          onClick={() => {
            setIsShowModal(false);
          }}
        >
          <div
            className="faq-inner"
            onClick={(event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
              event.stopPropagation();
            }}
          >
            <div className="container">
              <div className="d-flex justify-content-between">
                <h4 className="d-flex align-items-center">
                  {t("faq.title")}

                  <img src="/images/icon-faq.png" alt="" />
                </h4>
                <span
                  style={{ marginRight: "58px" }}
                  onClick={() => {
                    setIsShowModal(false);
                    setIsQuestion(false);
                  }}
                >
                  x
                </span>
              </div>
              <div className="row align-items-center ">
                <div
                  className={
                    isQuestion
                      ? "col-11"
                      : "col-11 d-flex justify-content-between align-items-center"
                  }
                >
                  <div>
                    <p
                      style={{ cursor: "pointer" }}
                      onClick={() => setIsQuestion(true)}
                    >
                      {isQuestion ? (
                        <div
                          style={{ background: "#F7D9D8", padding: "5px 0px" }}
                        >
                          <b>{t("faq.question1Q")}</b>
                          <p className="mb-0">{t("faq.question1A")}</p>
                        </div>
                      ) : (
                        "--"
                      )}
                    </p>
                    <p>{t("faq.question2")}</p>
                    <p>{t("faq.question3")}</p>
                    <p style={{ marginBottom: "0px" }}>{t("faq.question4")}</p>
                  </div>
                  {isQuestion ? (
                    ""
                  ) : (
                    <div>
                      <img
                        style={{ width: "350px" }}
                        src="/images/faq.image.png"
                        alt=""
                      />
                    </div>
                  )}
                </div>
                <div className="col-1">
                  <p
                    style={{ cursor: "pointer" }}
                    onClick={() => setIsQuestion(false)}
                  >
                    <b>+</b>
                  </p>
                  <p>
                    <b>+</b>
                  </p>
                  <p>
                    <b>+</b>
                  </p>
                  <p style={{ marginBottom: "0px" }}>
                    <b>+</b>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </Faq>
      )}
    </>
  );
};

export default Header;

const Logos = styled(Marquee)`
  background-color: ${({ theme }) => theme.colors.custom2};

  height: 150px !important;
  align-items: center !important;
  img {
    width: 180px;
    height: 150px;
    margin-right: 100px;
  }
`;

const Navbar = styled.nav`
  background-color: white;
  box-shadow: rgba(0, 0, 0, 0.45) 0px 10px 20px -20px;

  .globe {
    width: 50px;
  }
  .logo {
    margin-bottom: -8px;
    background-color: none;
    width: 130px;
    box-shadow: rgba(0, 0, 0, 0.25) 0px 0.0625em 0.0625em,
      rgba(0, 0, 0, 0.25) 0px 0.125em 0.5em,
      rgba(255, 255, 255, 0.1) 0px 0px 0px 1px inset;
  }
  .active {
    border-bottom: 2px solid ${({ theme }) => theme.colors.customDarkRed};
  }
  .col-2.logo {
    padding: 0;
  }
  ul {
    margin-bottom: 0px;
  }
  ul li {
    display: inline-block;
    position: relative;
    font-weight: 600;
  }
  ul li a {
    display: block;
    text-decoration: none;
    color: ${({ theme }) => theme.colors.black};
    font-weight: 600;
  }
  ul li a:hover {
    color: ${({ theme }) => theme.colors.customRed};
  }
  ul li ul.dropdown {
    padding: 0px;
    width: auto;
    position: absolute;
    z-index: 999;
    display: none;
    padding-top: 10px;
    background: white;
  }
  ul li:hover ul.dropdown {
    display: block;
  }
  ul li ul.dropdown li {
    display: block;
    width: 165px;
    padding: 2px 0px 2px 4px;
  }
  ul li ul.dropdown li:hover {
    background-color: ${({ theme }) => theme.colors.custom2};
    font-weight: 600;
  }
`;

const Faq = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100vh;
  background-color: rgba(0, 0, 0, 0.2);
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 99;

  h4 {
    color: ${({ theme }) => theme.colors.customRed};
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: 600;
  }
  h4 img {
    width: 50px;
    margin-left: 10px;
    margin-right: 260px;
  }
  span {
    color: black;
    cursor: pointer;
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    font-weight: 600;
  }
  .faq-inner {
    width: 100%;
    background-color: white;
    z-index: 99;
    min-height: 45vh;
  }
`;
