import React, { useEffect, useState } from "react";
import styled from "styled-components";

const ScrollToTop = () => {
  const [backToTop, setBackToTop] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 100) {
        setBackToTop(true);
      } else {
        setBackToTop(false);
      }
    });
  }, []);
  const scrollUp = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };
  return (
    <>
      {backToTop && (
        <Scroll onClick={scrollUp}>
          <img src="/icons/arrow-up.png" alt="" />
        </Scroll>
      )}
    </>
  );
};

export default ScrollToTop;
const Scroll = styled.div`
  position: fixed;
  bottom: 50px;
  right: 10px;
  padding: 4px 2px 0px 2px;
  outline: 2px solid gray;
  border: 2px solid gray;
  border-radius: 50%;
  cursor: pointer;
  z-index: 999;
  img {
    width: 30px;
  }
`;
