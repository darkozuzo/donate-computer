import React from "react";
import styled from "styled-components";

const OurMission = () => {
  return (
    <Container className="container">
      <div className="row justify-content-between align-items-center">
        <div className="col-4">
          <h3>
            НАШАТА <span>МИСИЈА</span>
          </h3>
          <h2>Биди пример!</h2>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis
            veritatis magni quod rerum velit minima quo iusto, ut commodi
            accusantium consectetur voluptatum impedit est incidunt? Commodi
            tempore saepe id incidunt.
          </p>
        </div>

        <div className="col-8 bg-img">
          <img src="/images/aboutUs-missionBg.png" alt="" />
        </div>
      </div>
    </Container>
  );
};

export default OurMission;

const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXL}px 0;
  img {
    margin-left: 20px;
  }
  /* .bg-img {
    background-image: url("/images/aboutUs-missionBg.png");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 700px; */
  /* } */
  h3 {
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    font-weight: 600;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
  h2 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: 600;
  }
`;
