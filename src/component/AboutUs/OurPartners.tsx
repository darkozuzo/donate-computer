import React, { useState } from "react";
import Accordion from "react-bootstrap/Accordion";
import styled from "styled-components";
import { Partners } from "../../types";
interface Props {
  macedonia: Partners[];
}

const OurPartners = ({ macedonia }: Props) => {
  const [isActiveOne, setIsActiveOne] = useState<boolean>(false);
  const [isActiveTwo, setIsActiveTwo] = useState<boolean>(false);
  const [isActiveThree, setIsActiveThree] = useState<boolean>(false);

  return (
    <Container>
      <div className="container">
        <div className="row justify-content-center align-items-center">
          <div className="col-4 d-flex align-items-center align-items-center flex-column text-center">
            <img src="/images/golden-snitch.svg" alt="" />
            <h2>
              НАШИ<span> ПАРТНЕРИ</span>
            </h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque,
              mollitia ut.
            </p>
          </div>
        </div>
        <div className="row justify-content-center align-items-center">
          <div className="col-5">
            <AccordionX>
              <div
                className={`${isActiveOne ? "contentBx active" : "contentBx"}`}
                onClick={() => {
                  setIsActiveOne(!isActiveOne);
                }}
              >
                <div className="label">Македонија</div>
                <div className="content">
                  <ol>
                    {macedonia.map((mk, index) => {
                      return <li key={index}>{mk.company}</li>;
                    })}
                  </ol>
                </div>
              </div>
              <div
                className={`${isActiveTwo ? "contentBx active" : "contentBx"}`}
                onClick={() => {
                  setIsActiveTwo(!isActiveTwo);
                }}
              >
                <div className="label">Интернационални</div>
                <div className="content">
                  <ol>
                    <li>nestp</li>
                    <li>nestp</li>
                    <li>nestp</li>
                    <li>nestp</li>
                    <li>nestp</li>
                    <li>nestp</li>
                    <li>nestp</li>
                    <li>nestp</li>
                    <li>nestp</li>
                  </ol>
                </div>
              </div>
              <div
                className={`${
                  isActiveThree ? "contentBx active" : "contentBx"
                }`}
                onClick={() => {
                  setIsActiveThree((prevState) => !prevState);
                }}
              >
                <div className="label">Официјални спонзори</div>
                <div className="content">
                  <ol>
                    <li>nestp</li>
                    <li>nestp</li>
                    <li>nestp</li>
                    <li>nestp</li>
                    <li>nestp</li>
                    <li>nestp</li>
                    <li>nestp</li>
                    <li>nestp</li>
                    <li>nestp</li>
                  </ol>
                </div>
              </div>
            </AccordionX>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default OurPartners;

const Container = styled.div`
  background-color: ${({ theme }) => theme.colors.custom2};
  padding: ${({ theme }) => theme.spacers.XXL}px 0px;
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
  h2 {
    font-size: ${({ theme }) => theme.fonts.font_50};
    font-weight: 600;
    margin-bottom: ${({ theme }) => theme.spacers.XL}px;
  }
  img {
    margin-bottom: ${({ theme }) => theme.spacers.XL}px;
  }
`;
const AccordionX = styled.div`
  .contentBx {
    position: relative;
    margin: 10px 20px;
  }
  .contentBx.active .content {
    height: 150px;
    border: 1px solid black;
    padding: 5px;
  }
  .contentBx .label {
    position: relative;
    padding: 10px;
    background-color: ${({ theme }) => theme.colors.customRed};
    color: white;
    cursor: pointer;
  }
  .contentBx.active .label::before {
    content: "-";
  }
  .contentBx .label::before {
    content: "+";
    position: absolute;
    top: 50%;
    right: 5%;
    transform: translateY(-50%);
  }
  .contentBx .content {
    position: relative;
    height: 0;
    overflow: hidden;
    transition: 0.5s;
    overflow-y: auto;
  }
`;
