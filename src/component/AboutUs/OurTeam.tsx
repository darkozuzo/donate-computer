import React from "react";
import styled from "styled-components";

const OurTeam = () => {
  return (
    <Container className="container">
      <div className="row text-center justify-content-center">
        <h2>
          НАШИОТ <span>ТИМ</span>
        </h2>
        <div className="col-3">
          <div className="bgColor">
            <p>
              Борче
              <br></br>
              <span>occupation</span>
            </p>
            <img src="/images/team-1.png" alt="" />
          </div>
        </div>
        <div className="col-3 left-right">
          <div className="bgColor">
            <p>
              Рената<br></br> <span>occupation</span>
            </p>
            <img src="/images/team-2.png" alt="" />
          </div>
        </div>

        <div className="col-3">
          <div className="bgColor">
            <p>
              Друг<br></br> <span>occupation</span>
            </p>
            <img src="/images/team-3.png" alt="" />
          </div>
        </div>
      </div>
    </Container>
  );
};

export default OurTeam;

const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXL}px;
  .col-3 {
    padding: 40px;
  }
  .bgColor {
    padding: 20px;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
  h2 {
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    font-weight: ${({ theme }) => theme.weights.weight_600};
  }
  img {
    margin: 0 auto;
    width: 100px;
    height: 100px;
  }
  .bgColor {
    background-color: ${({ theme }) => theme.colors.custom2};
  }
  .left-right {
    margin: 0 50px;
  }
`;
