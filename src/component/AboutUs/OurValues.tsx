import React from "react";
import styled from "styled-components";

const OurValues = () => {
  return (
    <Container className="container">
      <div className="row text-center justify-content-center">
        <h2>
          НАШИТЕ <span>ВРЕДНОСТИ</span>
        </h2>
        <div className="col-3">
          <div className="bgColor">
            <img src="/images/value-1.png" alt="" />
            <p>
              Ние сме<br></br>скромни
            </p>
          </div>
        </div>
        <div className="col-3 left-right">
          <div className="bgColor">
            <img src="/images/value-2.png" alt="" />
            <p>
              Го сакаме тоа<br></br>што го правиме
            </p>
          </div>
        </div>

        <div className="col-3">
          <div className="bgColor">
            <img src="/images/value-3.png" alt="" />
            <p>
              Полни сме<br></br>со почит
            </p>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default OurValues;

const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXL}px;
  .col-3 {
    padding: 40px;
  }
  .bgColor {
    padding: 20px;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
  h2 {
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    font-weight: ${({ theme }) => theme.weights.weight_600};
  }
  img {
    margin: 0 auto;
    width: 100px;
    height: 100px;
  }
  .bgColor {
    background-color: ${({ theme }) => theme.colors.custom2};
  }
  .left-right {
    margin: 0 50px;
  }
  p {
    font-size: ${({ theme }) => theme.fonts.font_20}px;
    font-weight: 600;
  }
`;
