import React, { useState } from "react";
import styled, { keyframes } from "styled-components";
interface Props {
  progress: {
    computers: string;
    tablets: string;
  };
}

const Progress: React.FC<Props> = ({ progress }) => {
  const [isSkopje, setIsSkopje] = useState<boolean>(false);
  const [isStip, setIsStip] = useState<boolean>(false);
  const [isGostivar, setIsGostivar] = useState<boolean>(false);
  const [isKavadarci, setIsKavadarci] = useState<boolean>(false);
  const [isStrumica, setIsStrumica] = useState<boolean>(false);
  const [isOhrid, setIsOhrid] = useState<boolean>(false);
  const [isBitola, setIsBitola] = useState<boolean>(false);
  return (
    <Container className="container">
      <div className="row justify-content-center align-items-center">
        <div className="col-7">
          <div className="position-relative">
            <img src="/images/map-illustration-MKD.png" alt="" />
            <Pin1
              onMouseEnter={() => setIsSkopje(true)}
              onMouseLeave={() => setIsSkopje(false)}
              className="position-absolute d-flex align-items-center"
              style={{ cursor: "pointer" }}
            >
              <img src="/images/gifts/location-pin.gif" alt="" />
              <p>Скопје</p>
              <br></br>
              {isSkopje && <span>1200 Динирани компјутери</span>}
            </Pin1>
            <Pin2
              style={{ cursor: "pointer" }}
              className="position-absolute d-flex align-items-center"
              onMouseEnter={() => setIsGostivar(true)}
              onMouseLeave={() => setIsGostivar(false)}
            >
              <img src="/images/gifts/location-pin.gif" alt="" />
              <p>Гостивар</p>
              <br></br>
              {isGostivar && (
                <span>
                  1200 <br></br>Динирани компјутери
                </span>
              )}
            </Pin2>
            <Pin3
              onMouseEnter={() => setIsStip(true)}
              onMouseLeave={() => setIsStip(false)}
              style={{
                cursor: "pointer",
              }}
              className="position-absolute d-flex align-items-center"
            >
              <img src="/images/gifts/location-pin.gif" alt="" />
              <p>Штип</p>

              {isStip && (
                <span>
                  1200 <br></br>Динирани компјутери
                </span>
              )}
            </Pin3>
            <Pin4
              onMouseEnter={() => setIsKavadarci(true)}
              onMouseLeave={() => setIsKavadarci(false)}
              style={{
                cursor: "pointer",
              }}
              className="position-absolute d-flex align-items-center"
            >
              <img src="/images/gifts/location-pin.gif" alt="" />
              <p>Кавадарци</p>
              <br></br>
              {isKavadarci && (
                <span>
                  1200 <br></br>Динирани компјутери
                </span>
              )}
            </Pin4>
            <Pin5
              className="position-absolute d-flex align-items-center"
              onMouseEnter={() => setIsStrumica(true)}
              onMouseLeave={() => setIsStrumica(false)}
              style={{
                cursor: "pointer",
              }}
            >
              <img src="/images/gifts/location-pin.gif" alt="" />
              <p>Струмица</p>
              {isStrumica && (
                <span>
                  1200 <br></br>Динирани компјутери
                </span>
              )}
            </Pin5>
            <Pin6
              className="position-absolute d-flex align-items-center"
              onMouseEnter={() => setIsOhrid(true)}
              onMouseLeave={() => setIsOhrid(false)}
              style={{
                cursor: "pointer",
              }}
            >
              <img src="/images/gifts/location-pin.gif" alt="" />
              <p>Охрид</p>
              {isOhrid && (
                <span>
                  1200 <br></br>Динирани компјутери
                </span>
              )}
            </Pin6>
            <Pin7
              className="position-absolute d-flex align-items-center"
              onMouseEnter={() => setIsBitola(true)}
              onMouseLeave={() => setIsBitola(false)}
              style={{
                cursor: "pointer",
              }}
            >
              <img src="/images/gifts/location-pin.gif" alt="" />
              <p>Битола</p>
              {isBitola && (
                <span>
                  1200 <br></br>Динирани компјутери
                </span>
              )}
            </Pin7>
          </div>
        </div>
        <BackgroundImg className="col-5 position-relative">
          <div className="position-absolute absolute">
            <img src="/images/heart-truck.png" alt="" />
            <p>{progress.computers} компјутери</p>
            <p>{progress.tablets} таблети</p>
          </div>
        </BackgroundImg>
      </div>
    </Container>
  );
};

export default Progress;
const Container = styled.div`
  padding: ${({ theme }) => theme.colors.XXL}px 0px;

  span {
    position: absolute;
    top: 50px;
    left: 20px;
    text-align: center;
    font-size: 8px;
    /* background-color: ${({ theme }) => theme.colors.custom4};
    color: white; */
    background-color: white;
    padding-top: 10px;
    clip-path: polygon(
      0 40%,
      30% 28%,
      50% 2%,
      70% 26%,
      100% 40%,
      100% 100%,
      0 100%
    );
    font-weight: 600;
  }
`;

const BackgroundImg = styled.div`
  background-image: url("images/progress-bg.png");
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  height: 350px;
  .absolute {
    top: 100px;
    left: 10px;
  }
`;

const Pin1 = styled.div`
  top: 80px;
  left: 230px;

  img {
    width: 40px;
    z-index: 9;
  }
  p {
    margin-left: -20px;
    background-color: white;
    border-radius: 10px;
    padding: 0 15px;
    margin-top: 37px;
    font-size: 8px;
    font-weight: 600;
  }
`;
const Pin2 = styled.div`
  top: 130px;
  left: 130px;
  img {
    width: 40px;
    z-index: 9;
  }
  p {
    margin-left: -20px;
    background-color: white;
    border-radius: 10px;
    padding: 0 15px;
    margin-top: 37px;
    font-size: 8px;
    font-weight: 600;
  }
`;
const Pin3 = styled.div`
  top: 120px;
  left: 385px;
  img {
    width: 40px;
    z-index: 9;
  }
  p {
    margin-left: -20px;
    background-color: white;
    border-radius: 10px;
    padding: 0 15px;
    margin-top: 37px;
    font-size: 8px;
    font-weight: 600;
  }
`;
const Pin4 = styled.div`
  top: 200px;
  left: 355px;
  img {
    width: 40px;
    z-index: 9;
  }
  p {
    margin-left: -20px;
    background-color: white;
    border-radius: 10px;
    padding: 0 15px;
    margin-top: 37px;
    font-size: 8px;
    font-weight: 600;
  }
`;
const Pin5 = styled.div`
  top: 200px;
  left: 490px;
  img {
    width: 40px;
    z-index: 9;
  }
  p {
    margin-left: -20px;
    background-color: white;
    border-radius: 10px;
    padding: 0 15px;
    margin-top: 37px;
    font-size: 8px;
    font-weight: 600;
  }
`;
const Pin6 = styled.div`
  top: 250px;
  left: 105px;
  img {
    width: 40px;
    z-index: 9;
  }
  p {
    margin-left: -20px;
    background-color: white;
    border-radius: 10px;
    padding: 0 15px;
    margin-top: 37px;
    font-size: 8px;
    font-weight: 600;
  }
`;
const Pin7 = styled.div`
  top: 270px;
  left: 195px;
  img {
    width: 40px;
    z-index: 9;
  }
  p {
    margin-left: -20px;
    background-color: white;
    border-radius: 10px;
    padding: 0 15px;
    margin-top: 37px;
    font-size: 8px;
    font-weight: 600;
  }
`;
