import React from "react";
import styled from "styled-components";
import Button from "../../commonComponents/Button";
import Link from "next/link";

const Volunteer = () => {
  return (
    <VolunteerContent className=" position-relative">
      <div className="container">
        <div className="row justify-content-center align-items-center">
          <div className="col-7">
            <h3>ВОЛОНТИРАЈ</h3>
            <h2>
              Спремен си да направиш<br></br>
              <span>промена?</span>
            </h2>
          </div>
          <div className="col-5">
            <Link href="/volunteering">
              <Button variant="variantOne">volontiraj</Button>
            </Link>
            <img src="/vectors/volunteer-1.svg" alt="" />
          </div>
        </div>
      </div>
    </VolunteerContent>
  );
};

export default Volunteer;

const VolunteerContent = styled.div`
  background-color: ${({ theme }) => theme.colors.custom2};
  padding: ${({ theme }) => theme.spacers.XXXL}px;
  &::before {
    content: "";
    position: absolute;
    top: -5px;
    left: 0%;
    width: 50%;
    border-bottom: 10px dotted ${({ theme }) => theme.colors.customRed};
  }
  &::after {
    content: "";
    position: absolute;
    bottom: -5px;
    right: 0%;
    width: 50%;
    border-bottom: 10px dotted ${({ theme }) => theme.colors.customRed};
  }
  button {
    margin-bottom: 10px;
  }
  img {
    width: 170px;
    height: 80px;
    margin-left: 5px;
  }
  h3 {
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    font-weight: 600;
  }
  h2 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: 600;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
`;
