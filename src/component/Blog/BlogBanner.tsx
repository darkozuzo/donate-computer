import Link from "next/link";
import React from "react";
import styled from "styled-components";

const BlogBanner = () => {
  return (
    <Banner className="container text-center">
      <h1>
        БИДИ ВО ТЕК<span>СО НАС</span>!
      </h1>
      <div className="row">
        <div className="col-4">
          <img src="/vectors/blog-banner-left.svg" alt="" />
        </div>
        <div className="col-4"></div>
        <div className="col-4">
          <img src="/vectors/blog-banner-right.svg" alt="" />
        </div>
      </div>
      <div className="row justify-content-center">
        <BannerIcons className="col-4 d-flex  justify-content-around align-items-center">
          <LinkedIn href={"http://www.linkedIn.com"} target="_blank">
            <img src="/icons/linked-in.png" alt="" />
          </LinkedIn>
          <Facebook href={"http://www.facebook.com"} target="_blank">
            <img className="p-1" src="/icons/facebook.png" alt="" />
          </Facebook>
          <Instagram href={"http://www.instagram.com"} target="_blank">
            <img src="/icons/instagram.png" alt="" />
          </Instagram>
          <Twitter href={"https://www.twitter.com"} target="_blank">
            <img src="/icons/twitter.png" alt="" />
          </Twitter>
        </BannerIcons>
      </div>
    </Banner>
  );
};

export default BlogBanner;

const Banner = styled.div`
  margin-top: 114px;
  padding: ${({ theme }) => theme.spacers.XXXL}px 0;
  height: 60vh;

  h1 {
    font-size: ${({ theme }) => theme.fonts.font_72}px;
    font-weight: 800;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
`;

const BannerIcons = styled.div`
  margin: -60px;
  padding: 0 70px;
  img {
    margin: 0 auto;
  }
`;

const Twitter = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;
const Instagram = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;
const Facebook = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;
const LinkedIn = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;
