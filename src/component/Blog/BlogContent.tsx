import React from "react";
import styled from "styled-components";
import { CardOne } from "../../types";
import { NextPage } from "next";
import Link from "next/link";
import Slider from "react-slick";
import CardVariantOne from "../../commonComponents/CardVariantOne";
import { useRouter } from "next/router";
interface Props {
  variant: CardOne;
  card: CardOne[];
}

const BlogContent: NextPage<Props> = ({ variant, card }) => {
  const router = useRouter();
  const settings = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "0px",
    slidesToShow: 3,
    speed: 500,
    rows: 1,
    slidesPerRow: 2,
  };
  return (
    <Container className="container">
      <div className="row">
        <div className="col-12 d-flex justify-content-end p-0">
          <img src="/icons/filter-icon.png" alt="" />
        </div>
        <Left className="col-6 position-relative">
          <img src={variant.img} alt="" />
          <div className="borderLeft"></div>
        </Left>
        <Right className="col-6 ">
          <div className="borderRight"></div>
          <div className="inner">
            <p>{variant.text}</p>
            <div className="d-flex justify-content-between align-items-between align-items-center">
              <p>{variant.date}</p>
              <LinkRed href={`/blog/${variant.id}`}>Learn more</LinkRed>
            </div>
          </div>
        </Right>
        <div className="col-12 py-5 px-0 d-flex flex-column justifu-content-center">
          <Slider {...settings}>
            {card.map((c) => {
              return <CardVariantOne key={c.id} {...c} />;
            })}
          </Slider>
        </div>
      </div>
    </Container>
  );
};

export default BlogContent;
const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXXL}px 0
    ${({ theme }) => theme.spacers.XL}px 0;
  .col-6 {
    padding: 0;
  }
  .inner {
    padding: 20px;
  }
  .card {
    margin-bottom: 10px !important;
    margin: 0 auto;
  }
  .slick-prev:before {
    content: url("/icons/left-icon.svg") !important;
  }
  .slick-next:before {
    content: url("/icons/right-icon.svg") !important;
  }
  .slick-prev {
    left: 1000px;
    top: 880px;
  }
  .slick-next {
    right: 80px;
    top: 880px;
  }
  .slick-dots {
    display: none !important;
  }
`;
const Right = styled.div`
  background-color: ${({ theme }) => theme.colors.custom2};
  .borderRight {
    border-top: 10px dotted ${({ theme }) => theme.colors.customRed};
    width: 50%;
    margin-left: 50%;
    margin-top: 10px;
  }
`;

const Left = styled.div`
  .borderLeft {
    position: absolute;
    top: 10px;
    left: 0;
    border-top: 10px dotted ${({ theme }) => theme.colors.customRed};
    width: 50%;
    margin-right: 50%;
  }
`;

const LinkRed = styled(Link)`
  color: ${({ theme }) => theme.colors.customRed};
`;
