import React from "react";
import Slider from "react-slick";
import styled from "styled-components";
import { CardOne } from "../../types";
import CardVariantOne from "../../commonComponents/CardVariantOne";
interface Props {
  cardOne: CardOne[];
}

const Donors: React.FC<Props> = ({ cardOne }) => {
  const settings = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "0px",
    slidesToShow: 3,
    speed: 500,
  };
  return (
    <Container className="container">
      <Inner>
        <h2>Наши донатори</h2>
        <First>
          <img src="/vectors/donors.svg" alt="" />
        </First>
        <Second>
          <img src="/vectors/donors-2.svg" alt="" />
        </Second>
      </Inner>
      <div className="row justify-content-center align-items-center">
        <div className="col-10">
          <Slide {...settings}>
            {cardOne?.map((card) => {
              return <CardVariantOne key={card.id} {...card} />;
            })}
          </Slide>
        </div>
      </div>
    </Container>
  );
};

export default Donors;

const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXXL}px 0;
`;

const Slide = styled(Slider)`
  .slick-next:before {
    content: url("/icons/right-icon.svg") !important;
  }
  .slick-prev:before {
    content: url("/icons/left-icon.svg") !important;
  }
  .slick-prev {
    top: 440px;
    left: 800px;
  }
  .slick-next {
    top: 440px;
    right: 60px;
  }
  .slick-dots {
    display: none !important;
  }
`;

const Inner = styled.div`
  margin-left: 95px;
  margin-bottom: ${({ theme }) => theme.spacers.XXXL}px;
  position: relative;
  h2 {
    font-weight: 600;
  }
`;
const First = styled.div`
  position: absolute;
  left: -50px;
  top: -35px;
  width: 350px;
`;
const Second = styled.div`
  position: absolute;
  left: 266px;
  top: -25px;
  width: 300px;
`;
