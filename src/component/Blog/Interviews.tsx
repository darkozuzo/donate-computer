import { url } from "inspector";
import { NextPage } from "next";
import React from "react";
import Slider from "react-slick";
import styled from "styled-components";

const Interviews: NextPage = () => {
  const settings = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "0px",
    slidesToShow: 3,
    speed: 500,
  };
  return (
    <ParentCont
      style={{
        backgroundImage: "url(/images/bg-images/bg-interviews.png)",
        backgroundSize: "cover",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        height: "430px",
      }}
    >
      <Container className="container">
        <div className="row justify-content-center align-items-center">
          <div className="col-11">
            <Slide {...settings}>
              <div>
                <iframe
                  width="311px"
                  height="207px"
                  src="https://www.youtube.com/embed/VJkEURwqtAM"
                  title="YouTube video player"
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                  allowFullScreen
                ></iframe>
              </div>
              <div>
                <iframe
                  width="311px"
                  height="207px"
                  src="https://www.youtube.com/embed/VJkEURwqtAM"
                  title="YouTube video player"
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                  allowFullScreen
                ></iframe>
              </div>
              <div>
                <iframe
                  width="311px"
                  height="207px"
                  src="https://www.youtube.com/embed/VJkEURwqtAM"
                  title="YouTube video player"
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                  allowFullScreen
                ></iframe>
              </div>
              <div>
                <iframe
                  width="311px"
                  height="207px"
                  src="https://www.youtube.com/embed/VJkEURwqtAM"
                  title="YouTube video player"
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                  allowFullScreen
                ></iframe>
              </div>
              <div>
                <iframe
                  width="311px"
                  height="207px"
                  src="https://www.youtube.com/embed/VJkEURwqtAM"
                  title="YouTube video player"
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                  allowFullScreen
                ></iframe>
              </div>
              <div>
                <iframe
                  width="311px"
                  height="207px"
                  src="https://www.youtube.com/embed/VJkEURwqtAM"
                  title="YouTube video player"
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                  allowFullScreen
                ></iframe>
              </div>
            </Slide>
          </div>
        </div>
      </Container>
      <h2>Интервјуа</h2>
    </ParentCont>
  );
};

export default Interviews;

const ParentCont = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  position: relative;
  padding: ${({ theme }) => theme.spacers.XXL}px 0;
  h2 {
    position: absolute;
    left: 100px;
    top: 0;
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: 600;
  }
`;

const Container = styled.div``;

const Slide = styled(Slider)`
  .slick-next:before {
    content: url("/icons/right-icon.svg") !important;
  }
  .slick-prev:before {
    content: url("/icons/left-icon.svg") !important;
  }
  .slick-prev {
    left: -90px;
    top: 95px;
  }
  .slick-next {
    right: -40px;
    top: 95px;
  }
  .slick-dots {
    display: none !important;
  }
  .slick-center iframe {
    transform: scale(1.2);
  }
  iframe {
    padding: 20px;
  }
`;
