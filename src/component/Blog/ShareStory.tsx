import React from "react";
import styled from "styled-components";
import Button from "../../commonComponents/Button";
import Link from "next/link";

const ShareStory = () => {
  return (
    <>
      <Title>
        <h3>
          Сподели ја
          <br />
          <span> тввојата приказна</span>
        </h3>
        <img src="/vectors/share-story-underTitle.svg" alt="" />
      </Title>
      <Container>
        <Left>
          <img src="/images/share-story-left.png" alt="" />
        </Left>
        <Center>
          <img src="/images/share-story-center.png" alt="" />
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit nisi
            quaerat accusamus ipsam sit recusandae! Dolores numquam porro
            nesciunt. Sint numquam dignissimos sit sequi alias. Iste sint
            doloremque nisi eos?
          </p>
          <Link href="/contactUs">
            <Button variant="variantOne">Сподели со нас</Button>
          </Link>
        </Center>
        <Right>
          <img src="/images/share-story-right.png" alt="" />
        </Right>
      </Container>
    </>
  );
};

export default ShareStory;
const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
const Left = styled.div`
  flex-basis: 33.33%;
  padding: 0 20px 0 0;
`;
const Center = styled.div`
  flex-basis: 33.33%;
  padding-bottom: 20px;
  text-align: center;
  img {
    margin-bottom: ${({ theme }) => theme.spacers.XXXL}px;
  }
  p {
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
`;
const Right = styled.div`
  flex-basis: 33.33%;
  padding: 0 0 0 20px;
  text-align: right;
`;

const Title = styled.div`
  margin-left: 160px;
  h3 {
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    font-weight: 600;
    margin-bottom: 5px;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
  img {
    margin-left: 40px;
    height: 20px;
  }
`;
