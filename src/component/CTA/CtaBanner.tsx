import React from "react";
import styled from "styled-components";
import Button from "../../commonComponents/Button";
import Link from "next/link";

const CtaBanner = () => {
  return (
    <>
      <Container className=" text-center">
        <h1>
          ДА СИ ПОМОГНЕМЕ<span> ЕДЕН НА ДРУГ</span>
        </h1>
        <CtaBg></CtaBg>
        <div className=" d-flex justify-content-center align-items-center buttons">
          <Link href="/donate">
            <ButtonLeft variant="variantOne">Донирај за компјутер</ButtonLeft>
          </Link>
          <Link href="/applyForPc-Eq">
            <Button variant="variantFour">Аплицирај за компјутер</Button>
          </Link>
        </div>
      </Container>
    </>
  );
};

export default CtaBanner;
const Container = styled.div`
  margin-top: 114px;
  padding: ${({ theme }) => theme.spacers.XXXL}px 0;
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
  h1 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: 800;
  }
  .space {
    width: 30px;
  }
`;

const CtaBg = styled.div`
  background-image: url("/vectors/cta-bg-1.svg");
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  width: 100%;
  height: 100vh;
  margin-left: 20px;
`;

const ButtonLeft = styled(Button)`
  margin-right: 30px;
`;
