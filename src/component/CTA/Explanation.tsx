import React, { useState } from "react";
import styled from "styled-components";
import { theme } from "../../styles/theme";

const Explanation = () => {
  const [active, setActive] = useState<string>("left");
  return (
    <>
      <ExplanationProcess>
        <div
          onClick={() => {
            setActive("left");
          }}
          style={{
            background: `${active === "left" ? theme.colors.custom2 : ""}`,
            textAlign: "center",
            width: "50%",
            padding: "15px",
            cursor: "pointer",
          }}
        >
          <b>Како да донираш?</b>
        </div>
        <div
          onClick={() => {
            setActive("right");
          }}
          style={{
            background: `${active === "right" ? theme.colors.custom2 : ""}`,
            textAlign: "center",
            width: "50%",
            padding: "15px",
            cursor: "pointer",
          }}
        >
          <b>Како да добиеш компјутер?</b>
        </div>
      </ExplanationProcess>

      {active === "left" ? (
        <div
          className="d-flex justify-content-center align-items-center flex-column text-center"
          style={{
            backgroundColor: theme.colors.custom2,
          }}
        >
          <BgLeft className="position-relative">
            <div>
              <img src="/images/bg-images/cta-bg-4.png" alt="" />
            </div>
          </BgLeft>
          <Paragraph>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam earum
            totam, delectus<br></br> vel distinctio dolorem quos quibusdam
            veniam. Animi exercitationem autem<br></br> repellendus enim quasi
            placeat nam debitis? Nemo, quam illum.
          </Paragraph>
        </div>
      ) : (
        <div
          className="d-flex justify-content-center align-items-center flex-column text-center"
          style={{
            backgroundColor: theme.colors.custom2,
          }}
        >
          <BgRight></BgRight>
          <Paragraph>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam earum
            totam, delectus<br></br> vel distinctio dolorem quos quibusdam
            veniam. Animi exercitationem autem<br></br> repellendus enim quasi
            placeat nam debitis? Nemo, quam illum.
          </Paragraph>
        </div>
      )}
    </>
  );
};

export default Explanation;

const ExplanationProcess = styled.div`
  display: flex;
`;

const BgLeft = styled.div`
  background-image: url("/vectors/cta-bg-2.svg");
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  width: 70%;
  height: 50vh;
  background-color: ${({ theme }) => theme.colors.custom2};
  div {
    position: absolute;
    top: 100px;
    left: 170px;
  }
  img {
    width: 530px;
  }
`;

const BgRight = styled.div`
  background-image: url("/vectors/cta-bg-3.svg");
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  width: 70%;
  height: 50vh;
  background-color: ${({ theme }) => theme.colors.custom2};
`;

const Paragraph = styled.p`
  background-color: ${({ theme }) => theme.colors.custom2};
  padding: 20px;
`;
