import Link from "next/link";
import React from "react";
import Button from "../../commonComponents/Button";
import styled from "styled-components";

const OtherWays = () => {
  return (
    <Container className="container text-center">
      <h2>
        Бараш друг начин на кој можеш да ни<br></br>помогнеш?
      </h2>
      <div className="row text-center justify-content-center align-items-center">
        <div className="col-8">
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur rem
            nisi inventore odio sed distinctio qui numquam, incidunt aperiam
            minima! Ad eos a nulla repellendus, impedit minima numquam alias
            velit! Lorem, ipsum dolor sit amet consectetur adipisicing elit.
            Nihil eius quae nobis sunt est beatae saepe minus ab doloremque
            praesentium, corrupti error quisquam quibusdam? Nostrum sapiente
            modi veniam possimus eum.
          </p>
          <div className="mb-3">
            <Link href="/volunteering">
              <ButtonLeft variant="variantOne">Волонтирај</ButtonLeft>
            </Link>
            <Link href="/contributeDiff">
              <Button variant="variantFour">Придонеси Поинаку</Button>
            </Link>
          </div>
          <Link href="/contactUs">Остави коментар</Link>
        </div>
      </div>
    </Container>
  );
};

export default OtherWays;

const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXL}px 0;
  h2 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
    font-weight: 600;
  }
  p {
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
`;
const ButtonLeft = styled(Button)`
  padding: 10px 80px;
  margin-right: 40px;
`;
