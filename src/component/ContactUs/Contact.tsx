import React, { useState } from "react";
import styled from "styled-components";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Button from "../../commonComponents/Button";
import Link from "next/link";
// type FormType = {
//   name: string;
//   lastName: string;
//   email: string;
//   tel: any;
//   message: string;
// };

const Contact = () => {
  // const [formData, setFormData] = useState<FormType>({
  //   name: "",
  //   lastName: "",
  //   email: "",
  //   tel: "",
  //   message: "",
  // });
  // const handleChange = (
  //   event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  // ) => {
  //   event.preventDefault();
  //   console.log(event.target.value);
  // };
  const [name, setName] = useState<string>("");
  const [lastName, setLastName] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [telNumber, setTelNumber] = useState<number | string>("");
  const [textArea, setTextArea] = useState<string | number>();

  return (
    <Container className="container">
      <div className="row">
        <ContactLeft className="col-6">
          <h2>
            Стапи во<br></br> контакт <span>со нас!</span>
          </h2>
          <p>
            <b> Телефонски број:</b> <br></br> (+389)071/228-802
          </p>
          <p>
            <b>Е-маил:</b> <br></br> donirajkompjuter@gmail.com
          </p>
          <p>
            <b>Адреса:</b> <br></br> ул. Браќа Хаџи Теови бр.38 <br></br>{" "}
            Кавадарци
          </p>
          <BgImg>
            <h4>Нептун локации</h4>
            <Link href="https://www.google.com/search?rlz=1C1GCEA_enMK883MK883&tbs=lrf:!1m4!1u3!2m2!3m1!1e1!2m1!1e3!3sIAE,lf:1,lf_ui:4&tbm=lcl&sxsrf=AJOqlzUpOix3_l3wy6P_SxxuA3G8a96Aug:1679504993178&q=neptun%20lokacija&rflfq=1&num=10&sa=X&ved=2ahUKEwjt_pDUg_D9AhWnQ_EDHWdIAxcQjGp6BAgaEAE&cshid=1679505199332825&biw=1707&bih=821&dpr=0.8&rlst=f#rlfi=hd:;si:;mv:[[42.143600299999996,21.7360698],[41.9873141,21.4601686]];tbs:lrf:!1m4!1u3!2m2!3m1!1e1!2m1!1e3!3sIAE,lf:1,lf_ui:4">
              линк за Нептун локации
            </Link>
          </BgImg>
        </ContactLeft>
        <ContactRight className="col-6">
          <Form
            className="p-5"
            onSubmit={(event: React.FormEvent<HTMLFormElement>) => {
              event.preventDefault();
              console.log(name, lastName, email, telNumber, textArea);
              setEmail("");
              setLastName("");
              setName("");
              setTelNumber("");
              setTextArea("");
            }}
          >
            <div className="row">
              <div className="col-6 mb-4">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Име"
                  value={name}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                    setName(event.target.value)
                  }
                />
              </div>
              <div className="col-6 mb-4">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Презиме"
                  value={lastName}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                    setLastName(event.target.value)
                  }
                />
              </div>
              <div className="col-12 mb-4">
                <input
                  type="email"
                  className="form-control"
                  placeholder="Емаил Адреса"
                  value={email}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                    setEmail(event.target.value)
                  }
                />
              </div>
              <div className="col-12 mb-4">
                <input
                  type="tel"
                  className="form-control"
                  placeholder="Телефонски Број"
                  value={telNumber}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                    setTelNumber(event.target.value)
                  }
                />
              </div>
              <div className="col-12 mb-4">
                <textarea
                  className="form-control"
                  rows={3}
                  value={textArea}
                  onChange={(event: React.ChangeEvent<HTMLTextAreaElement>) => {
                    setTextArea(event.target.value);
                  }}
                ></textarea>
              </div>
            </div>

            <Button variant="variantFour" type="submit">
              Испрати
            </Button>
          </Form>
        </ContactRight>
      </div>
    </Container>
  );
};

export default Contact;
const Container = styled.div`
  margin-top: 114px;
  padding: ${({ theme }) => theme.spacers.XXL}px 0 0 0;
  h2 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: 600;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
`;
const ContactRight = styled.div`
  form {
    background-color: ${({ theme }) => theme.colors.custom2};
  }
`;

const ContactLeft = styled.div`
  img {
    width: 300px;
  }
`;

const BgImg = styled.div`
  width: 100%;
  height: 320px;
  background-image: url("/images/contact-bg.png");
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
`;

// const ContactLeftInner = styled.div``;
