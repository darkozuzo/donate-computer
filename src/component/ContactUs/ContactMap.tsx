import React from "react";
import styled from "styled-components";

const ContactMap = () => {
  return <BgMap></BgMap>;
};

export default ContactMap;
const BgMap = styled.div`
  background-image: url("/images/contact-bg-2.png");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  width: 100%;
  height: 450px;
`;
