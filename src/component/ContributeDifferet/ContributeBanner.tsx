import React from "react";
import styled from "styled-components";

const ContributeBanner = () => {
  return (
    <Banner className="text-center position-relative">
      <h1>
        ЗАПОЗНАЈ СЕ СО ТИМОТ ОД <br></br>
        <span> ДОНИРАЈ КОМПЈУТЕР</span>
      </h1>
      <div className="computer position-absolute">
        <img src="/images/gifts/rotate-monitor.gif" alt="" />
      </div>
    </Banner>
  );
};

export default ContributeBanner;
const Banner = styled.div`
  height: 70vh;
  margin-top: 114px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-bottom: 10px dotted ${({ theme }) => theme.colors.customRed};

  h1 {
    font-size: ${({ theme }) => theme.fonts.font_72}px;
    line-height: 84px;
    font-weight: ${({ theme }) => theme.weights.weight_800};
  }
  img {
    width: 180px;
  }
  .computer {
    left: 50%;
    bottom: -15px;
    transform: translate(-50%, 50%);
    background-color: white;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
`;
