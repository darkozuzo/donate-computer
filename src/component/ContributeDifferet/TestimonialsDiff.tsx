import React from "react";
import Button from "../../commonComponents/Button";
import styled from "styled-components";
import Link from "next/link";

const TestimonialsDiff = () => {
  return (
    <Container className="container">
      <Row className="row justify-content-center align-items-center text-center">
        <h5>Одбери еден од многуте начини со кои можеш да си помогнеш</h5>
        <div className="col-8">
          <RowAverage className="row align-items-center text-start">
            <div className="col-3">
              <img src="/vectors/contribute-1.svg" alt="" />
            </div>
            <div className="col-9">
              <span>
                <h6>Дали патуваш често и би сакал да пренесеш компјутер?</h6>
              </span>
              <p>
                Ако патуваш и сакаш да ни помогнеш во разнесување на
                компјутерите за оние на кои им е потребно, контактирај не’ и ќе
                бидеме многу среќни што учевствуваш во нашата мисија.
              </p>
              <p>Среќен Пат!</p>
            </div>
          </RowAverage>
          <RowAverage className="row align-items-center text-start">
            <div className="col-3">
              <img src="/vectors/contribute-2.svg" alt="" />
            </div>
            <div className="col-9">
              <span>
                <h6>Прошири ја нашата приказна</h6>
              </span>
              <p>
                Дали уживаш да раскажуваш значајни приказни? Објасни го
                концептот на Донирај Компјутер и сподели го твоето искуство на
                твојата фамилија и пријатели, означи не на социјалните мрежи и
                помогни во растењето на нашиот успех.
              </p>
            </div>
          </RowAverage>
          <RowAverage className="row align-items-center text-start">
            <div className="col-3">
              <img src="/vectors/contribute-3.svg" alt="" />
            </div>
            <div className="col-9">
              <span>
                <h6>Дали знаеш да преведуваш на повеќе јазици?</h6>
              </span>
              <p>
                Ако си преведувач или знаеш добро некој јазик, можеш да ни
                помогнеш во преведување на што повеќе јазици за целиот свет да
                се запознае со нас.
              </p>
            </div>
          </RowAverage>
          <RowAverage className="row align-items-center text-start">
            <div className="col-3">
              <img src="/vectors/contribute-4.svg" alt="" />
            </div>
            <div className="col-9">
              <span>
                <h6>Рециклирај ги твоите уреди</h6>
              </span>
              <p>
                Благодарение на <span className="zero">Zero Waste</span> можеш
                да се ослободиш од уредите кои не ги користиш така што тие ќе
                бидат рециклирани и повторно употребни.
              </p>
              <p>Направи и услуга на природата!</p>
            </div>
          </RowAverage>
          <RowAverage className="row align-items-center text-start">
            <div className="col-3">
              <img src="/vectors/contribute-5.svg" alt="" />
            </div>
            <div className="col-9">
              <span>
                <h6>Имаш желба да ги споделиш твоите компјутерски вештини?</h6>
              </span>
              <p>
                Со твоите вештини можеш да помогнеш и да ги едуцираш оние на кои
                тоа им треба. Вклучи се во нашата нова акција.
              </p>
              <p>Дознај повеќе во полето Едукација на нашата страна.</p>
            </div>
          </RowAverage>
          <RowAverage className="row">
            <div className="col">
              <h6>
                Ако се пронаоѓаш во едно или повеќе понудени опции,<br></br>{" "}
                тогаш не се двоуми и стани дел од нашата успешна<br></br>{" "}
                приказна!
              </h6>
              <br></br>
              <Link href="/contactUs">
                <Button variant="variantOne">Контактирај не</Button>
              </Link>
            </div>
          </RowAverage>
        </div>
      </Row>
    </Container>
  );
};

export default TestimonialsDiff;

const Container = styled.div`
  h5 {
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    font-weight: 600;
    margin-bottom: ${({ theme }) => theme.spacers.XXXL}px;
  }
  h6 {
    font-size: ${({ theme }) => theme.fonts.font_24}px;
    font-weight: 600;
  }
  img {
    width: 200px;
    margin: 0 auto;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
  span.zero {
    color: inherit;
    text-decoration: underline;
  }
`;
const Row = styled.div`
  padding: calc(${({ theme }) => theme.spacers.XXXL * 2}px) 0
    ${({ theme }) => theme.spacers.XXL}px 0;
`;
const RowAverage = styled.div`
  padding: ${({ theme }) => theme.spacers.XXL}px 0;
`;
