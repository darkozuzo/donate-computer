import React from "react";
import Button from "../../commonComponents/Button";
import styled from "styled-components";
import { NextPage } from "next";
import Link from "next/link";

const EducateYourself: NextPage = () => {
  return (
    <Container className="container">
      <div className="row">
        <div className="col-6 d-flex flex-column text-center align-items-center justify-content-center ">
          <h4>
            Вклучи<span> се!</span>
          </h4>
          <h3>Сакаш да едуцираш?</h3>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat,
            exercitationem.
          </p>
          <Link href="/contactUs">
            <Button variant="variantOne">Контактирај не</Button>
          </Link>
        </div>
        <div className="col-6 right">
          <img src="/images/educate-yourself.png" alt="" />
        </div>
      </div>
    </Container>
  );
};

export default EducateYourself;

const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXL}px 0;
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
  h3 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: 600;
  }
  h4 {
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    font-weight: 600;
  }
  .right img {
    width: 70%;
  }
`;
