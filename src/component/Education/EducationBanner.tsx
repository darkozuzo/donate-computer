import React, { useState } from "react";
import styled from "styled-components";
import Button from "../../commonComponents/Button";
import { Link } from "react-scroll";

const EducationBanner = () => {
  const [isStartVideo, setIsStartVideo] = useState<boolean>(false);
  // const handleScroll = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
  //   e.preventDefault();
  //   const href = e.currentTarget.href;
  //   const targetId = href.replace(/.*\#/, "");
  //   const elem = document.getElementById(targetId);
  //   elem?.scrollIntoView({
  //     behavior: "smooth",
  //     block: "start",
  //   });
  //   window.scrollBy(0, 950); // Adjust the value (-100) to set the desired offset from the top
  // };
  return (
    <ParentContainer className="text-center">
      <h1>
        ДОЗНАЈ ПОВЕЌЕ ЗА <br></br> <span>ДИГИТАЛНА ИЛУЗИЈА</span>
      </h1>
      <Container
        className="container position-relative"
        isStartVideo={isStartVideo}
      >
        <div className="row justify-content-center align-items-center text-center">
          <div
            className="col d-flex flex-column justify-content-center align-items-center"
            style={{
              backgroundImage: "url(/images/bg-images/education-banner.png)",
              backgroundPosition: "center",
              backgroundSize: "contain",
              backgroundRepeat: "no-repeat",
              height: "500px",
              width: "100%",
            }}
          >
            <ParentButton className="d-flex">
              <Link
                to="function"
                spy={true}
                smooth={true}
                duration={400}
                offset={-100}
                onClick={() => setIsStartVideo(false)}
              >
                <Button variant="variantOne">Започни</Button>
              </Link>
              <Link
                to="center-video"
                spy={true}
                smooth={true}
                duration={5}
                offset={-227}
              >
                <PlayButton
                  id="center-video"
                  className="d-flex align-items-center justify-content-center"
                  onClick={() => {
                    setIsStartVideo(true);
                  }}
                >
                  <div>
                    <img src="/icons/play-button.png" alt="" />
                  </div>
                  <b>Watch Video</b>
                </PlayButton>
              </Link>
            </ParentButton>
            {isStartVideo && (
              <div className="d-flex video embed-responsive embed-responsive-16by9">
                <iframe
                  className="iframe embed-responsive-item "
                  src="https://www.youtube.com/embed/kQF_g9ovtBg?rel=0"
                  allowFullScreen
                ></iframe>
                <div
                  onClick={() => setIsStartVideo(false)}
                  className="closeVideo"
                >
                  x
                </div>
              </div>
            )}
            <div className="people" id="people">
              <img src="/images/education-banner-people.png" alt="" />
            </div>
          </div>
        </div>
        <div className="position-absolute one">
          <img src="/vectors/marketing-banner-1.png" alt="" />
        </div>
        <div className="position-absolute two">
          <img src="/vectors/marketing-banner-2.png" alt="" />
        </div>
      </Container>
      <Sponsors className="container text-center">
        <p>sponsored by:</p>
        <div className="row justify-content-center align-items-center">
          <div className="col-3">
            <img src="images/logo-images/education-banner-logo-1.png" alt="" />
          </div>
          <div className="col-3">
            <img src="images/logo-images/education-banner-logo-2.png" alt="" />
          </div>
          <div className="col-3">
            <img src="images/logo-images/education-banner-logo-1.png" alt="" />
          </div>
          <div className="col-3">
            <img src="images/logo-images/education-banner-logo-3.png" alt="" />
          </div>
        </div>
      </Sponsors>
    </ParentContainer>
  );
};

export default EducationBanner;

const ParentContainer = styled.div`
  margin-top: 114px;
  width: 100%;
  padding: ${({ theme }) => theme.spacers.XXXL}px 0;
  border-bottom: 10px dotted ${({ theme }) => theme.colors.customRed};

  h1 {
    font-size: ${({ theme }) => theme.fonts.font_72}px;
    line-height: 84px;
    font-weight: ${({ theme }) => theme.weights.weight_800};
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
`;

const Container = styled.div<{ isStartVideo: any }>`
  height: max-content;
  .iframe {
    border-radius: 13px;
  }
  .video {
    /* padding: 8px 0px 8px 11px; */
    /* border-radius: 13px; */
    /* border: 2px solid ${({ theme }) => theme.colors.customRed}; */
    /* background-color: rgba(215, 63, 59, 0.5); */
  }
  .closeVideo {
    cursor: pointer;
    margin-left: -20px;
    margin-right: 20px;
    font-size: 20px;
    color: white;
  }
  .people {
    width: 50%;
  }
  .one {
    width: 140px;
    left: 417px;
    top: ${(props) => (props.isStartVideo ? "-10px" : "70px")};
    z-index: -1;
  }
  .two {
    width: 80px;
    left: 550px;
    top: ${(props) => (props.isStartVideo ? "48px" : "125px")};
    z-index: -1;
  }
`;

const PlayButton = styled.div`
  cursor: pointer;
  margin-left: 30px;
  img {
    width: 40px;
  }
`;
const ParentButton = styled.div`
  margin-bottom: ${({ theme }) => theme.spacers.XXXL}px;
`;
const Sponsors = styled.div`
  padding-top: ${({ theme }) => theme.spacers.XL}px;
  p {
    margin-bottom: ${({ theme }) => theme.spacers.XXXL}px;
    color: ${({ theme }) => theme.colors.custom1};
  }
  img {
    width: 100px;
    margin: 0 auto;
  }
`;
