import React from "react";
import styled from "styled-components";

const HowDoesItFunction = () => {
  return (
    <Container className="container" id="function">
      <div className="row text-center">
        <h3>
          КАКО <span>ФУНКЦИОНИРА?</span>
        </h3>
        <div className="col-3 text-start">
          <div className="bgColor">
            <img src="/icons/education-icon-1.png" alt="" />
            <p>
              <b>Релаксирачко</b>
            </p>
            <p>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ea,
              beatae.
            </p>
          </div>
        </div>
        <div className="col-3 text-start">
          <div className="bgColor">
            <img src="/icons/education-icon-2.png" alt="" />
            <p>
              <b>Квалитет</b>
            </p>
            <p>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ea,
              beatae.
            </p>
          </div>
        </div>
        <div className="col-3 text-start">
          <div className="bgColor">
            <img src="/icons/education-icon-3.png" alt="" />
            <p>
              <b>Креативно размислување</b>
            </p>
            <p>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ea,
              beatae.
            </p>
          </div>
        </div>
        <div className="col-3 text-start">
          <div className="bgColor">
            <img src="/icons/education-icon-4.png" alt="" />
            <p>
              <b>Сертификат</b>
            </p>
            <p>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ea,
              beatae.
            </p>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default HowDoesItFunction;

const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXXL}px 0;
  h3 {
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    font-weight: 600;
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
  img {
    width: 50px;
    margin-bottom: ${({ theme }) => theme.spacers.XL}px;
  }
  .bgColor {
    background-color: ${({ theme }) => theme.colors.custom2};
    border-radius: 20px;
    padding: 30px;
  }
  p {
    margin-bottom: ${({ theme }) => theme.spacers.XL}px;
  }
`;
