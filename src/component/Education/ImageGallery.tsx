import React from "react";
import Slider from "react-slick";
import styled from "styled-components";
import { ImageType } from "../../types";
interface Props {
  marketing: ImageType[];
}

const ImageGallery: React.FC<Props> = ({ marketing }) => {
  const settings = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "10px",
    slidesToShow: 3,
    speed: 500,
  };
  return (
    <Container className="container">
      <div className="row justify-content-center">
        <div className="col-10">
          <Slide {...settings}>
            {marketing.map((image) => {
              return (
                <div key={image.id} className="p-4">
                  <img src={image.img} alt="" />
                </div>
              );
            })}
          </Slide>
        </div>
      </div>
    </Container>
  );
};

export default ImageGallery;

const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXXL}px 0;
`;

const Slide = styled(Slider)`
  .slick-next:before {
    content: url("/icons/right-icon.svg") !important;
  }
  .slick-prev:before {
    content: url("/icons/left-icon.svg") !important;
  }
  .slick-prev {
    left: -90px;
    top: 95px;
  }
  .slick-next {
    right: -40px;
    top: 95px;
  }
  .slick-dots {
    display: none !important;
  }
  .slick-center img {
    transform: scale(1.2);
  }
`;
