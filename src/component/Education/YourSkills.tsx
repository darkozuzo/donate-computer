import React from "react";
import styled from "styled-components";

const YourSkills = () => {
  return (
    <>
      <Container className="container">
        <h3>
          Tvoite <span>Vestini</span>
        </h3>
        <div className="row justify-content-center align-items-center">
          <div className="col-10">
            <div className="row justify-content-center align-items-center">
              <div className="col-4 d-flex justify-content-between align-items-center">
                <div className="first-left">
                  <p>Одбери</p>
                  <div className="first-left-inner">
                    <img src="/vectors/first-left-inner-svg.svg" alt="" />
                  </div>
                </div>
                <div className="second-right">
                  <p>Основно Ниво</p>
                  <p>Напредно ниво</p>
                </div>
              </div>
              <Background className="col-8">
                <div className="row d-flex justify-content-center align-items-center">
                  <div className="col-8">
                    <p>
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Similique laboriosam culpa minima assumenda? Quo numquam
                      odio id consequatur libero labore sunt exercitationem
                      expedita est vel veniam eos laborum, hic harum!
                    </p>
                    <PlayButton className="d-flex align-items-center justify-content-start ">
                      <div>
                        <img src="/icons/play-button.png" alt="" />
                      </div>
                      <b>Watch Video</b>
                    </PlayButton>
                  </div>
                </div>
              </Background>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
};

export default YourSkills;
const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXXL}px 0;

  h3 {
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    font-weight: 600;
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
    margin-left: 90px;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
  .first-left {
    text-align: start;
    font-size: ${({ theme }) => theme.fonts.font_20}px;
    position: relative;
  }
  .first-left-inner {
    position: absolute;
    top: 0px;
    left: -16px;
    width: 120px;
  }
  .first-left-inner img {
    width: 200px;
  }
  .second-right p {
    margin-bottom: 60px;
    margin-top: 60px;
    font-size: ${({ theme }) => theme.fonts.font_20}px;
  }
  .second-right {
    background-color: ${({ theme }) => theme.colors.custom2};
    padding: 10px;
    border-radius: 40px;
    box-shadow: 0 4px 2px -1px black;
  }
`;
const Background = styled.div`
  background-image: url("/images/bg-images/education-bg.png");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 260px;
  display: flex;
  justify-content: center;
  align-items: center;
  p {
    padding-top: 40px;
  }
`;

const PlayButton = styled.div`
  cursor: pointer;
  img {
    width: 40px;
  }
`;
