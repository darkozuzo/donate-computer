import React from "react";
import "react-multi-carousel/lib/styles.css";
import { useRouter } from "next/router";
import Slider from "react-slick";
import styled from "styled-components";
import CardVariantOne from "../../commonComponents/CardVariantOne";
import { CardOne } from "../../types";
interface Props {
  cardOne: CardOne[];
}

const CarouselHomePage: React.FC<Props> = ({ cardOne }) => {
  const router = useRouter();

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
  };
  return (
    <Container className="container text-center">
      <div className="row justify-content-center align-items-center">
        <div className="col-10">
          <Slide {...settings}>
            {cardOne?.map((card) => {
              return <CardVariantOne key={card.id} {...card} />;
            })}
          </Slide>
        </div>
      </div>
    </Container>
  );
};

export default CarouselHomePage;

const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXXL}px 0;
`;

const Slide = styled(Slider)`
  .slick-next:before {
    content: url("/icons/right-icon.svg") !important;
  }
  .slick-prev:before {
    content: url("/icons/left-icon.svg") !important;
  }
  .slick-prev {
    left: -90px;
  }
  .slick-next {
    right: -40px;
  }
  .slick-dots {
    display: none !important;
  }
`;
