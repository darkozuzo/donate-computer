import React, { useState } from "react";
import styled from "styled-components";
import CountUp from "react-countup";
import ScrollTrigger from "react-scroll-trigger";
interface Props {
  clock1: {
    years: string;
    title: string;
  };
  clock2: {
    computers: string;
    title: string;
  };
  clock3: {
    without_computer: string;
    title: string;
  };
}

const ClockPage = ({ clock1, clock2, clock3 }: Props) => {
  const [counterOn, setCounterOn] = useState(false);
  return (
    <Clock className="border-left border-right position-relative d-flex justify-content-center align-items-center">
      <div className="text-center d-flex justify-content-center align-items-center flex-column">
        <img style={{ width: "190px" }} src="../images/Clock1.png" alt="" />
        <p>{clock1.title}</p>
        <div className="clock-1">
          <ScrollTrigger
            onEnter={() => setCounterOn(true)}
            onExit={() => setCounterOn(false)}
          >
            <b>
              {counterOn && (
                <>
                  <CountUp
                    start={0}
                    end={+clock1.years}
                    duration={4}
                    delay={0}
                  />
                  +
                </>
              )}
            </b>
          </ScrollTrigger>
        </div>
      </div>
      <div className="text-center d-flex justify-content-center align-items-center flex-column">
        <img style={{ width: "110px" }} src="../images/Clock2.png" alt="" />
        <p>{clock2.title}</p>
        <div className="clock-2">
          <ScrollTrigger
            onEnter={() => setCounterOn(true)}
            onExit={() => setCounterOn(false)}
          >
            <b>
              {counterOn && (
                <>
                  <CountUp
                    start={0}
                    end={+clock2.computers}
                    duration={4}
                    delay={0}
                  />
                  +
                </>
              )}
            </b>
          </ScrollTrigger>
        </div>
      </div>
      <div className="text-center d-flex justify-content-center align-items-center flex-column  ">
        <img style={{ width: "150px" }} src="../images/Clock3.png" alt="" />
        <p>{clock3.title}</p>
        <div className="clock-3">
          <ScrollTrigger
            onEnter={() => setCounterOn(true)}
            onExit={() => setCounterOn(false)}
          >
            <b>
              {counterOn && (
                <>
                  <CountUp
                    start={0}
                    end={+clock3.without_computer}
                    duration={4}
                    delay={0}
                  />
                  +
                </>
              )}
            </b>
          </ScrollTrigger>
        </div>
      </div>
    </Clock>
  );
};

export default ClockPage;

const Clock = styled.div`
  background-color: ${({ theme }) => theme.colors.custom2};
  padding: ${({ theme }) => theme.spacers.XXXL}px;
  img {
    margin-bottom: 10px;
  }
  div {
    flex-basis: 28%;
    position: relative;
  }
  .clock-1 {
    position: absolute;
    top: 30px;
    right: 120px;
  }
  .clock-2 {
    position: absolute;
    top: 28px;
    right: 130px;
  }
  .clock-3 {
    position: absolute;
    top: 37px;
    right: 100px;
  }
`;
