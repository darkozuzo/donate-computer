import React from "react";
import styled from "styled-components";
import Button from "../../commonComponents/Button";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";

const HomeBannerPage = () => {
  const { t } = useTranslation("common");
  return (
    <>
      <HomeBanner className=" d-flex justify-content-between position-relative">
        <img src="../images/banner-images/home-banner-left.jpg" alt="" />
        <img
          className="mb-5"
          src="../images/banner-images/home-banner-right.jpg"
          alt=""
        />
        <div className="homeBanner-inner  position-absolute">
          <h1>
            {t("homePageBanner.homeBannerTitle1")}{" "}
            <span>{t("homePageBanner.homeBannerTitle2")}</span>
            <br></br>
            {t("homePageBanner.homeBannerTitle3")}
            <br></br>
            <span>{t("homePageBanner.homeBannerTitle4")} </span>
            {t("homePageBanner.homeBannerTitle5")}
          </h1>
          <Link href="/donate">
            <Button variant="variantOne">
              {t("homePageBanner.homePageButtonLeft")}
            </Button>
          </Link>

          <Link href="/applyForPc-Eq">
            <Button variant="variantThree">
              {t("homePageBanner.homePageButtonRight")}
            </Button>
          </Link>
        </div>
      </HomeBanner>
    </>
  );
};

export default HomeBannerPage;

const HomeBanner = styled.div`
  height: 100vh;
  padding: ${({ theme }) => theme.spacers.XXXL}px 0 0 0;

  .homeBanner-inner {
    top: 50%;
    left: 5%;
    transform: translateY(-50%);
  }
  button {
    margin-right: ${({ theme }) => theme.spacers.L}px;
  }
  h1 {
    font-size: ${({ theme }) => theme.fonts.font_72}px;
    line-height: 84px;
    font-weight: ${({ theme }) => theme.weights.weight_800};
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
`;
