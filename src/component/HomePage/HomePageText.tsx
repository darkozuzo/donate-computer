import React from "react";
import styled from "styled-components";
import useTranslation from "next-translate/useTranslation";

const HomePageText = () => {
  const { t } = useTranslation("common");
  return (
    <Container className="container">
      <div className="row justify-content-around align-items-center text-center">
        <div className="col-4">
          <h3>{t("homePageText.homePageTextLeft")}?</h3>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus
          ad officia impedit reprehenderit assumenda non, optio temporibus
          facilis veniam blanditiis inventore eligendi nihil! Ipsa doloribus
          repellendus tempore exercitationem hic ratione!
        </div>
        <div className="col-4">
          <h3>{t("homePageText.homePageTextRight")}?</h3>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus
          ad officia impedit reprehenderit assumenda non, optio temporibus
          facilis veniam blanditiis inventore eligendi nihil! Ipsa doloribus
          repellendus tempore exercitationem hic ratione!
        </div>
      </div>
    </Container>
  );
};

export default HomePageText;

const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXXL}px 0px;

  h3 {
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    font-weight: ${({ theme }) => theme.weights.weight_600};
    margin-bottom: ${({ theme }) => theme.spacers.XL}px;
  }
`;
