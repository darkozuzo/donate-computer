import React from "react";
import Button from "../../commonComponents/Button";
import styled from "styled-components";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";

const OurOffer = () => {
  const { t } = useTranslation("common");

  return (
    <Container className="container">
      <div className="row justify-content-center align-items-center text-center">
        <h3>{t("homePageOffer.title")}?</h3>
        <div className="col-3 text-center">
          <img src="./images/ourOffer-1.svg" alt="" />
          <Link href="/marketing">
            <Button variant="variantOne">{t("homePageOffer.button1")}</Button>
          </Link>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus
            reiciendis quas, at quisquam quasi ipsa ea rem tenetur dolores
            cumque ratione expedita libero id repudiandae architecto. Dolor
            ipsum veritatis quaerat!
          </p>
        </div>
        <div className="col-3 text-center">
          <img src="./images/ourOffer-2.svg" alt="" />
          <Link href="/education">
            <Button variant="variantOne">{t("homePageOffer.button2")}</Button>
          </Link>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus
            reiciendis quas, at quisquam quasi ipsa ea rem tenetur dolores
            cumque ratione expedita libero id repudiandae architecto. Dolor
            ipsum veritatis quaerat!
          </p>
        </div>
        <div className="col-3 text-center">
          <img src="./images/ourOffer-3.svg" alt="" />
          <Link href="volunteering">
            <Button variant="variantOne">{t("homePageOffer.button3")}</Button>
          </Link>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus
            reiciendis quas, at quisquam quasi ipsa ea rem tenetur dolores
            cumque ratione expedita libero id repudiandae architecto. Dolor
            ipsum veritatis quaerat!
          </p>
        </div>
      </div>
    </Container>
  );
};

export default OurOffer;

const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXXL}px 0;
  img {
    width: 190px;
    margin: 0 auto;
    margin-bottom: ${({ theme }) => theme.spacers.XL}px;
  }
  h3 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: ${({ theme }) => theme.weights.weight_600};
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  button {
    margin-bottom: ${({ theme }) => theme.spacers.XL}px;
  }
`;
