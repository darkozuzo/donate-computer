import React from "react";
import styled from "styled-components";
import { ImageType } from "../../types";
import Marquee from "react-fast-marquee";
interface Props {
  data: ImageType[];
}

const PartnerBannerSection = ({ data }: Props) => {
  return (
    <Marquee speed={150}>
      <PartnerBanner>
        {data.map((i) => {
          return (
            <div key={i.id}>
              <img src={i.img} alt="" />
            </div>
          );
        })}
      </PartnerBanner>
    </Marquee>
  );
};

export default PartnerBannerSection;

const PartnerBanner = styled.div`
  background-color: ${({ theme }) => theme.colors.custom2};
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 10px 0;

  img {
    width: 150px;
    margin-right: 50px;
  }
`;
