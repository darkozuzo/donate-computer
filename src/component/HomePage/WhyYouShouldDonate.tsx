import React from "react";
import styled from "styled-components";
import Button from "../../commonComponents/Button";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";

const WhyYouShouldDonate = () => {
  const { t } = useTranslation("common");

  return (
    <Container className="container text-center">
      <div className="row justify-content-center align-items-center text-center ">
        <h3>
          {t("whyYouShouldDonate.title")}{" "}
          <span>{t("whyYouShouldDonate.title2")}</span>?
        </h3>
        <div className="col-6 text-start">
          <div className="row justify-content-center align-items-end space">
            <div className="col-3">
              <img src="./images/whyYouShouldDonate-1.png" alt="" />
            </div>
            <div className="col-9">
              <p>{t("whyYouShouldDonate.donateText1")}</p>
              <p>{t("whyYouShouldDonate.donateText2")}.</p>
            </div>
          </div>
          <div className="row justify-content-center align-items-end space">
            <div className="col-3">
              <img src="./images/whyYouShouldDonate-2.png" alt="" />
            </div>
            <div className="col-9">
              <p>{t("whyYouShouldDonate.donateText3")}</p>
              <p>{t("whyYouShouldDonate.donateText4")}.</p>
            </div>
          </div>
          <div className="row justify-content-center align-items-end space">
            <div className="col-3">
              <img src="./images/whyYouShouldDonate-3.png" alt="" />
            </div>
            <div className="col-9">
              <p>{t("whyYouShouldDonate.donateText5")}</p>
              <p>{t("whyYouShouldDonate.donateText6")}.</p>
            </div>
          </div>
        </div>
      </div>
      <Link href="/donate">
        <Button variant="variantOne">
          {t("whyYouShouldDonate.donateButton")}
        </Button>
      </Link>
    </Container>
  );
};

export default WhyYouShouldDonate;

const Container = styled.div`
  background-image: url("/images/whyYouShouldDonate-bg.png");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  padding: ${({ theme }) => theme.spacers.XXL}px 0;
  h3 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: ${({ theme }) => theme.weights.weight_600};
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
  .space {
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  p {
    margin-bottom: 16px;
    font-size: ${({ theme }) => theme.fonts.font_20}px;
    font-weight: 400;
  }
`;
