import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { TestimonialsType } from "../../types";
import { useRouter } from "next/router";
interface Props {
  test: TestimonialsType[];
}

const Testimonials = ({ test }: Props) => {
  // const [firstElement, ...remainingElements] = test;
  // const [elements, setElements] = useState(remainingElements);
  const router = useRouter();
  const [handleTop, setHandleTop] = useState<TestimonialsType>(test[0]);
  const [handleRest, setHandleRest] = useState<TestimonialsType[]>([]);
  console.log(handleRest);
  // useEffect(() => {
  //   if (router.locale) {
  //     setHandleTop(test.find((el) => el?.id === handleTop?.id))!;
  //     setHandleRest(test.filter((t) => t.id !== handleTop?.id));
  //   }
  // }, [handleTop?.id, router.locale, test]);
  useEffect(() => {
    if (router.locale) {
      const newHandleTop = test.find((el) => el?.id === handleTop?.id);
      if (newHandleTop) {
        setHandleTop(newHandleTop);
        setHandleRest(test.filter((t) => t.id !== newHandleTop.id));
      }
    }
  }, [router.locale]);

  // const handleSwipe = (index: number) => {
  //   const updatedElements = [...elements];
  //   const clickedElement = updatedElements[index];
  //   updatedElements[index] = firstElement;
  //   test[0] = clickedElement;
  //   setElements(updatedElements);
  // };

  const topTop = ["150px", "280px", "150px", "280px"];
  const leftLeft = ["100px", "250px", "950px", "800px"];

  return (
    <Container className="container position-relative">
      <section className="d-flex justify-content-center flex-column align-items-center position-relative">
        <div className=" topImage imageStyle">
          <img src={handleTop?.img} alt="" />
        </div>
        <h6>{handleTop?.name}</h6>
        <span>
          <b>{handleTop?.age}</b>
        </span>
        <p>{handleTop?.text}</p>
      </section>
      {/* <section className="d-flex">
        {elements.map((element, index) => (
          <div
            key={element?.id}
            onClick={() => handleSwipe(index)}
            style={{ position: "absolute", top: "10", left: "10px" }}

          >
            <img src={element?.img} alt="" />
          </div>
        ))}
      </section> */}
      <section className="d-flex">
        {handleRest.map((el, index) => {
          return (
            <div
              className="imageStyle"
              style={{
                position: "absolute",
                top: topTop[index],
                left: leftLeft[index],
              }}
              key={el.id}
              onClick={() => {
                setHandleTop(el);
                handleRest.splice(index, 1, handleTop);
              }}
            >
              <img src={el.img} alt="" />
            </div>
          );
        })}
      </section>
    </Container>
  );
};

export default Testimonials;
const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXXL * 2}px 0;
  /* background-image: url("/images/Testimonials-bg.png");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 500px; */
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  h6 {
    padding-bottom: ${({ theme }) => theme.spacers.XL}px;
    font-weight: 600;
  }
  span {
    padding-bottom: ${({ theme }) => theme.spacers.XL}px;
  }
  img {
    cursor: pointer;
    padding-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  img:hover {
    margin-top: -3px;
  }
  .imageStyle {
    width: 90px;
    height: 90px;
    border-radius: 50%;
    box-shadow: 1px 5px 5px 0px rgba(0, 0, 0, 0.5);
    -webkit-box-shadow: 1px 5px 5px 0px rgba(0, 0, 0, 0.5);
    -moz-box-shadow: 1px 5px 5px 0px rgba(0, 0, 0, 0.5);
    background-color: rgba(0, 0, 0, 0.5);
    opacity: 0.9;
  }
  .imageStyle:hover {
    opacity: 1;
  }
  p {
    width: 40%;
    text-align: center;
  }
  .topImage {
    position: absolute;
    top: -120px;
    left: 520px;
  }
`;
