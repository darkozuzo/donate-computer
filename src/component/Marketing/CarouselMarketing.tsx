import React from "react";
import Slider from "react-slick";
import styled from "styled-components";
import { ImageType } from "../../types";
interface Props {
  commercial: ImageType[];
}

const CarouselMarketing: React.FC<Props> = ({ commercial }) => {
  console.log(commercial, "zuzo");
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
  };
  return (
    <Container className="container text-center">
      <div className="row justify-content-center align-items-center">
        <div className="col-8">
          <Slide {...settings}>
            {commercial.map((com) => {
              return (
                <span key={com.id} className="bgColor">
                  <img src={com.img} alt="" />
                </span>
              );
            })}
          </Slide>
        </div>
      </div>
    </Container>
  );
};

export default CarouselMarketing;

const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXXL}px 0;
`;

const Slide = styled(Slider)`
  .slick-next:before {
    content: url("/icons/right-icon.svg") !important;
  }
  .slick-prev:before {
    content: url("/icons/left-icon.svg") !important;
  }
  .slick-prev {
    left: -90px;
    top: 60px;
  }
  .slick-next {
    right: -40px;
    top: 60px;
  }
  .slick-dots {
    display: none !important;
  }
  .bgColor {
    background-color: ${({ theme }) => theme.colors.custom2};
    height: 155px;
    width: 155px !important;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
  }
  img {
    width: 130px;
    margin: 0 auto;
    margin-top: 24px;
  }
`;
