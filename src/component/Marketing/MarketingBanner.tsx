import React from "react";
import styled from "styled-components";
import Button from "../../commonComponents/Button";
import Link from "next/link";

const MarketingBanner = () => {
  return (
    <Container className="container d-flex justify-content-center align-items-center flex-column">
      <div className="text-center position-relative">
        <h1>
          НАПРАВИ ГО ТВОЈОТ БРЕНД <br></br>
          <span>ПРЕПОЗНАТЛИВ</span>
        </h1>
        <MarketingPosition>
          <Link href="/contactUs">
            <Button variant="variantOne">Промовирај се</Button>
          </Link>
          <div className="position-absolute one">
            <img src="/vectors/marketing-banner-1.png" alt="" />
          </div>
          <div className="position-absolute two">
            <img src="/vectors/marketing-banner-2.png" alt="" />
          </div>
        </MarketingPosition>
      </div>
    </Container>
  );
};

export default MarketingBanner;

const Container = styled.div`
  height: calc(100vh + 114px);
  width: 100%;
  overflow: hidden;
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
  h1 {
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
    font-size: ${({ theme }) => theme.fonts.font_72}px;
    line-height: 84px;
    font-weight: ${({ theme }) => theme.weights.weight_800};
  }
`;
const MarketingPosition = styled.div`
  width: 100%;

  .one {
    top: 155px;
    left: 430px;
    z-index: -1;
  }
  .two {
    /* margin-right: 0; */
    top: 220px;
    left: 570px;
    z-index: -1;
  }
`;
