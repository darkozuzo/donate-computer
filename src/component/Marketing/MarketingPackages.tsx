import React from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import styled from "styled-components";
import Button from "../../commonComponents/Button";
import Link from "next/link";

const MarketingPackages = () => {
  return (
    <Container className="container">
      <div className="row justify-content-center align-items-center"></div>
      <div className="row justify-content-center align-items-center">
        <div className="col-12 text-center">
          <h2>Нудиме голем избор на маркетинг пакети</h2>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda
            amet ratione voluptate nobis deserunt. <br></br> Lorem ipsum dolor
            sit amet consectetur adipisicing elit. Repellendus, error?
          </p>
          <Link href="/contactUs">
            <Button variant="variantOne">Промовирај се</Button>
          </Link>
        </div>
      </div>
    </Container>
  );
};

export default MarketingPackages;

const Container = styled.div`
  padding: ${({ theme }) => theme.spacers.XXL}px 0;
  h2 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
    font-weight: 600;
  }
  p {
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
`;
