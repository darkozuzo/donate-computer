import React from "react";
import styled from "styled-components";

const PromoteYourBrand = () => {
  return (
    <Container>
      <div className="container">
        <div className="row align-items-center">
          <div className="col-8">
            <h2>
              Зошто да го промовираш твојот <br></br>бренд на нашата страница
            </h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Doloribus, eaque ea? Recusandae, asperiores! Totam doloribus nemo
              reiciendis
            </p>
            <ul>
              <li>
                <img src="/icons/correct-icon.png" alt="" />{" "}
                <b>Lorem ipsum dolor sit amet consectetur.</b>
              </li>
              <li>
                <img src="/icons/correct-icon.png" alt="" />{" "}
                <b>Lorem ipsum dolor sit amet consectetur.</b>
              </li>
              <li>
                <img src="/icons/correct-icon.png" alt="" />
                <b>Lorem ipsum dolor sit amet consectetur.</b>
              </li>
            </ul>
          </div>
          <div className="col-4">
            <img src="/images/promote-1.svg" alt="" />
          </div>
        </div>
      </div>
    </Container>
  );
};

export default PromoteYourBrand;

const Container = styled.div`
  background-color: ${({ theme }) => theme.colors.custom3};
  padding: ${({ theme }) => theme.spacers.XXL}px 0;
  h2 {
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  p {
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  ul {
    margin: 0;
    padding: 0;
  }
  li {
    display: flex;
    align-items: center;
    margin-bottom: 10px;
  }
  li img {
    width: 20px;
    margin-right: 10px;
  }
`;
