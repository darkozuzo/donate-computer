import React from "react";
import Button from "../../commonComponents/Button";
import styled from "styled-components";
import Link from "next/link";
import { useRouter } from "next/router";

const Storytelling = () => {
  const router = useRouter();
  return (
    <>
      <ContainerTop className="container">
        <div className="row">
          <Left className="col-6">
            <img src="/vectors/volunteering-1.svg" alt="" />
            <h4>
              Помогни со донирање на <br></br> твоето време и идеи
            </h4>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Voluptatibus, eligendi. Lorem, ipsum dolor sit amet consectetur
              adipisicing elit. Lorem, ipsum dolor sit amet consectetur
              adipisicing elit.
            </p>
            {/* <img src="/images/volunteering-img-1.png" alt="" /> */}
          </Left>

          <Right className="col-6 text-end">
            <h4>Можеш да си дел од нас</h4>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Voluptatibus, eligendi. Lorem, ipsum dolor sit amet consectetur
              adipisicing elit.
            </p>
            <img src="/vectors/volunteering-2.svg" alt="" />
            {/* Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nihil
          voluptatem possimus quisquam maiores perspiciatis nemo reiciendis
          corrupti totam vitae voluptatibus est enim, magnam architecto quasi
          incidunt similique modi repudiandae. Repellat!<br></br>
          <Button>ostavi komentar</Button> */}
          </Right>
        </div>
      </ContainerTop>
      <ContainerBottom>
        <div className="container ">
          <div className="row">
            <div className="col-6">
              <img src="/images/volunteering-img-1.png" alt="" />
            </div>
            <div className="col-6 right">
              <h5>
                Ако си ведар, насмеан, полн со енергија, сакаш да помагаш и
                сакаш да бидеш дел од нашиот<span> тим</span> тогаш...
              </h5>
              <span>
                <p>
                  Вклучи се и ти во<br></br> приказната!
                </p>
              </span>
              <Link href={`${router.locale}/becameVolunteer`}>
                <Button variant="variantOne">Стани волонтер</Button>
              </Link>

              {/* <Button variant="variantOne">Стани волонтер</Button> */}
              <div className="circle-vector">
                <img src="/vectors/circle-vector.svg" alt="" />
              </div>
            </div>
          </div>
        </div>
      </ContainerBottom>
    </>
  );
};

export default Storytelling;

const ContainerTop = styled.div`
  h4 {
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    font-weight: 600;
    margin-bottom: ${({ theme }) => theme.spacers.XL}px;
  }
`;

const Left = styled.div`
  img {
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  p {
    padding-right: 140px;
  }
`;
const Right = styled.div`
  padding-top: calc(${({ theme }) => theme.spacers.XXL * 3}px);
  p {
    padding-left: 250px;
  }
  img {
    margin-top: ${({ theme }) => theme.spacers.XXL}px;
  }
`;
const ContainerBottom = styled.div`
  background-image: url("/vectors/volunteering-4.png");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  width: 100%;
  height: 650px;
  display: flex;
  justify-content: center;
  align-items: center;
  .right {
    text-align: right;
    position: relative;
  }
  h5 {
    font-size: ${({ theme }) => theme.fonts.font_24}px;
    font-weight: 600;
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
  span p {
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    font-weight: 600;
    margin-bottom: calc(${({ theme }) => theme.spacers.L * 19}px);
  }
  button {
    margin-right: calc(${({ theme }) => theme.spacers.XXL * 5}px);
  }
  .circle-vector {
    position: absolute;
    left: 180px;
    top: 210px;
    width: 300px;
    z-index: -1;
  }
`;
