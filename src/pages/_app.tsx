import "../styles/globals.css";
import "bootstrap/dist/css/bootstrap.css";
import type { AppProps } from "next/app";
import Header from "../commonComponents/Header";
import Footer from "../commonComponents/Footer";
import { ThemeProvider } from "styled-components";
import { theme } from "../styles/theme";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import { useState } from "react";

export default function App({ Component, pageProps }: AppProps) {
  const [isShowModal, setIsShowModal] = useState<boolean>(false);

  return (
    <div
      style={{
        height: isShowModal ? "100vh" : "auto",
        transition: "0.5s ease-in-out",
        overflow: isShowModal ? "hidden" : "auto",
      }}
    >
      <ThemeProvider theme={theme}>
        <Header isShowModal={isShowModal} setIsShowModal={setIsShowModal} />
        <Component {...pageProps} />
        <Footer />
      </ThemeProvider>
    </div>
  );
}
