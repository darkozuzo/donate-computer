import React from "react";
import Head from "next/head";
import OurMission from "../component/AboutUs/OurMission";
import OurValues from "../component/AboutUs/OurValues";
import OurTeam from "../component/AboutUs/OurTeam";
import Volunteer from "../component/AboutUs/Volunteer";
import Progress from "../component/AboutUs/Progress";
import OurPartners from "../component/AboutUs/OurPartners";
import CommonBanner from "../component/AboutUs/AboutUsBanner";
import BackgroundImg from "../commonComponents/BackgroundImg";
import { GetStaticProps, NextPage } from "next";
import CarouselAboutPage from "../component/AboutUs/CarouselAboutPage";
import { CardTwo, Partners } from "../types";
import ScrollToTop from "../commonComponents/scrollToTop";

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  const res = await fetch(`http://localhost:5000/${locale}`);
  const data = await res.json();
  return {
    props: {
      image: data.background_img.imgTwo,
      progress: data.progress,
      cardTwo: data.card.variant_two,
      macedonia: data.partnership.macedonia,
    },
  };
};

interface Props {
  image: string;
  progress: {
    computers: number;
    tablets: number;
  };
  cardTwo: CardTwo[];
  macedonia: Partners[];
}

const AboutUs: NextPage<Props> = ({ image, progress, cardTwo, macedonia }) => {
  return (
    <>
      <Head>
        <title>AboutUs</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <CommonBanner />
      <OurMission />
      <BackgroundImg image={image} />
      <OurValues />
      <OurTeam />
      <Volunteer />
      {/* @ts-ignore */}
      <Progress progress={progress} />
      <CarouselAboutPage cardTwo={cardTwo} />
      <OurPartners macedonia={macedonia} />
      <ScrollToTop />
    </>
  );
};

export default AboutUs;
