import React, { useState } from "react";
import styled from "styled-components";
import Button from "../commonComponents/Button";
import { useFormik } from "formik";
import Link from "next/link";
import ScrollToTop from "../commonComponents/scrollToTop";

const ApplyForPcEq = () => {
  const [value, setValue] = useState<string>("");
  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setValue(event.target.value);
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      lastName: "",
      city: "",
      email: "",
      tel: "",
      typePC: "",
      fileOne: "",
      fileTwo: "",
      detail: "",
    },
    onSubmit: (values) => {
      console.log("formik", values);
      setValue("done");
    },
    validate: (values) => {
      let errors = {};

      if (!values.name) {
        //@ts-ignore
        errors.name = "Required";
      }
      if (!values.lastName) {
        //@ts-ignore
        errors.lastName = "Required";
      }
      if (!values.city) {
        //@ts-ignore
        errors.city = "Required";
      }

      if (!values.email) {
        //@ts-ignore
        errors.email = "Required";
      } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
      ) {
        //@ts-ignore
        errors.email = "Invalid email address";
      }

      if (!values.tel) {
        //@ts-ignore
        errors.tel = "Required";
      }

      if (!values.typePC) {
        //@ts-ignore
        errors.typePC = "Required";
      }

      if (!values.fileOne) {
        //@ts-ignore
        errors.fileOne = "Required";
      }
      if (!values.fileTwo) {
        //@ts-ignore
        errors.fileTwo = "Required";
      }
      if (!values.detail) {
        //@ts-ignore
        errors.detail = "Required";
      }

      return errors;
    },
  });

  return (
    <>
      <Parent>
        <Container className="container">
          <div className="row">
            {value === "" ? (
              <>
                <div className="col-6 text-end align-self-center">
                  <h3>Ми треба...</h3>
                  <select value={value} onChange={handleChange}>
                    <option defaultValue={"choose"}>Одбери </option>
                    <option value="computer">Компјутер</option>
                    <option value="equipment">Опрема</option>
                  </select>
                </div>
                <div className="col-6 d-flex flex-column align-items-center">
                  <div className="hand">
                    <img src="/images/applay-1.png" alt="" />
                  </div>
                  <div>
                    <img src="/images/applay-2.png" alt="" />
                  </div>
                </div>
              </>
            ) : null}
            {value === "computer" ? (
              <div className="col-8 offset-2 text-center">
                <h4>Пред да аплицираш...</h4>
                <p className="mb-5">
                  computerLorem, ipsum dolor sit amet consectetur adipisicing
                  elit. Reiciendis deserunt nulla debitis aspernatur libero
                  possimus adipisci accusantium optio laborum corrupti, dicta
                  maiores odio rem iure doloribus labore nisi aut
                  exercitationem!
                </p>
                <ul>
                  <li>
                    <img src="/icons/correct-icon.png" alt="" />{" "}
                    <b>Lorem ipsum dolor sit amet consectetur.</b>
                  </li>
                  <li>
                    <img src="/icons/correct-icon.png" alt="" />{" "}
                    <b>Lorem ipsum dolor sit amet consectetur.</b>
                  </li>
                  <li>
                    <img src="/icons/correct-icon.png" alt="" />
                    <b>Lorem ipsum dolor sit amet consectetur.</b>
                  </li>
                </ul>
                <Buttons className="d-flex align-items-center justify-content-center">
                  <Button
                    className="modify"
                    variant="variantThree"
                    onClick={() => setValue("")}
                  >
                    <img src="/icons/arrow-back.png" alt="" />
                  </Button>
                  <Button
                    variant="variantOne"
                    onClick={() => setValue("computerForm")}
                  >
                    Продолжи
                  </Button>
                </Buttons>
              </div>
            ) : null}
            {value === "computerForm" ? (
              <div className="col-7">
                <h3>Еден чекор поблиску!</h3>
                <p>
                  Lorem ipsum dolor sit amet, consectetur <br />
                  adipisicing elit. Repellat, aut.
                </p>
                <form onSubmit={formik.handleSubmit}>
                  <div className="row my-5">
                    <div className="col-6">
                      <Input>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Име *"
                          name="name"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.name}
                        />
                        {formik.touched.name && formik.errors.name ? (
                          <span>{formik.errors.name}</span>
                        ) : null}
                      </Input>
                      <Input>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Презиме *"
                          name="lastName"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.lastName}
                        />
                        {formik.touched.lastName && formik.errors.lastName ? (
                          <span>{formik.errors.lastName}</span>
                        ) : null}
                      </Input>
                      <Input>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Град *"
                          name="city"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.city}
                        />
                        {formik.touched.city && formik.errors.city ? (
                          <span>{formik.errors.city}</span>
                        ) : null}
                      </Input>
                      <Input>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Маил *"
                          name="email"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.email}
                        />
                        {formik.touched.email && formik.errors.email ? (
                          <span>{formik.errors.email}</span>
                        ) : null}
                      </Input>
                      <Input>
                        <input
                          type="tel"
                          className="form-control"
                          placeholder="Телефонски Број *"
                          name="tel"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.tel}
                        />
                        {formik.touched.tel && formik.errors.tel ? (
                          <span>{formik.errors.tel}</span>
                        ) : null}
                      </Input>
                    </div>
                    <div className="col-6">
                      <Input>
                        <select
                          className="form-select w-100"
                          name="typePC"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.typePC}
                        >
                          <option value=" Тип на компјутер">
                            Тип на компјутер
                          </option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                        {formik.touched.typePC && formik.errors.typePC ? (
                          <span>{formik.errors.typePC}</span>
                        ) : null}
                      </Input>
                      <p>Можете да прикачите датотеки кои се потребни.</p>
                      <div className="d-flex">
                        <Input>
                          <input
                            type="text"
                            className="form-control w-86"
                            placeholder="+ Документ 1+"
                            name="fileOne"
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            value={formik.values.fileOne}
                          />
                          {formik.touched.fileOne && formik.errors.fileOne ? (
                            <span>{formik.errors.fileOne}</span>
                          ) : null}
                        </Input>
                        <Input>
                          <input
                            type="text"
                            className="form-control w-86"
                            placeholder="+ Документ 2"
                            name="fileTwo"
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            value={formik.values.fileTwo}
                          />
                          {formik.touched.fileTwo && formik.errors.fileTwo ? (
                            <span>{formik.errors.fileTwo}</span>
                          ) : null}
                        </Input>
                      </div>
                      <Input>
                        <textarea
                          className="form-control"
                          rows={3}
                          placeholder="Коментари  "
                          name="detail"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.detail}
                        />
                        {formik.touched.detail && formik.errors.detail ? (
                          <span>{formik.errors.detail}</span>
                        ) : null}
                      </Input>
                    </div>
                    <Buttons className="d-flex align-items-center justify-content-start">
                      <Button
                        className="modify"
                        variant="variantThree"
                        onClick={() => setValue("computer")}
                      >
                        <img src="/icons/arrow-back.png" alt="" />
                      </Button>
                      <Button variant="variantOne" type="submit">
                        Аплицирај
                      </Button>
                    </Buttons>
                  </div>
                </form>
              </div>
            ) : null}
            {value === "equipment" ? (
              <div className="col-8 offset-2 text-center">
                <h4>Пред да аплицираш...</h4>
                <p className="mb-5">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Reiciendis deserunt nulla debitis aspernatur libero possimus
                  adipisci accusantium optio laborum corrupti, dicta maiores
                  odio rem iure doloribus labore nisi aut exercitationem!
                </p>
                <ul>
                  <li>
                    <img src="/icons/correct-icon.png" alt="" />{" "}
                    <b>Lorem ipsum dolor sit amet consectetur.</b>
                  </li>
                  <li>
                    <img src="/icons/correct-icon.png" alt="" />{" "}
                    <b>Lorem ipsum dolor sit amet consectetur.</b>
                  </li>
                  <li>
                    <img src="/icons/correct-icon.png" alt="" />
                    <b>Lorem ipsum dolor sit amet consectetur.</b>
                  </li>
                </ul>
                <Buttons className="d-flex align-items-center justify-content-center">
                  <Button
                    className="modify"
                    variant="variantThree"
                    onClick={() => setValue("")}
                  >
                    <img src="/icons/arrow-back.png" alt="" />
                  </Button>
                  <Button
                    variant="variantOne"
                    onClick={() => setValue("equipmentForm")}
                  >
                    Продолжи
                  </Button>
                </Buttons>
              </div>
            ) : null}
            {value === "equipmentForm" ? (
              <div className="col-7">
                <h3>Еден чекор поблиску!</h3>
                <p>
                  Lorem ipsum dolor sit amet, consectetur <br />
                  adipisicing elit. Repellat, aut.
                </p>
                <form onSubmit={formik.handleSubmit}>
                  <div className="row my-5">
                    <div className="col-6">
                      <Input>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Име *"
                          name="name"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.name}
                        />
                        {formik.touched.name && formik.errors.name ? (
                          <span>{formik.errors.name}</span>
                        ) : null}
                      </Input>
                      <Input>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Презиме *"
                          name="lastName"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.lastName}
                        />
                        {formik.touched.lastName && formik.errors.lastName ? (
                          <span>{formik.errors.lastName}</span>
                        ) : null}
                      </Input>
                      <Input>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Град *"
                          name="city"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.city}
                        />
                        {formik.touched.city && formik.errors.city ? (
                          <span>{formik.errors.city}</span>
                        ) : null}
                      </Input>
                      <Input>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Маил *"
                          name="email"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.email}
                        />
                        {formik.touched.email && formik.errors.email ? (
                          <span>{formik.errors.email}</span>
                        ) : null}
                      </Input>
                      <Input>
                        <input
                          type="tel"
                          className="form-control"
                          placeholder="Телефонски Број *"
                          name="tel"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.tel}
                        />
                        {formik.touched.tel && formik.errors.tel ? (
                          <span>{formik.errors.tel}</span>
                        ) : null}
                      </Input>
                    </div>
                    <div className="col-6">
                      <Input>
                        <select
                          className="form-select w-100"
                          name="typePC"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.typePC}
                        >
                          <option value=" Тип на опрема">Тип на опрема</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                        {formik.touched.typePC && formik.errors.typePC ? (
                          <span>{formik.errors.typePC}</span>
                        ) : null}
                      </Input>
                      <p>Можете да прикачите датотеки кои се потребни.</p>
                      <div className="d-flex">
                        <Input>
                          <input
                            type="text"
                            className="form-control w-86"
                            placeholder="+ Документ 1+"
                            name="fileOne"
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            value={formik.values.fileOne}
                          />
                          {formik.touched.fileOne && formik.errors.fileOne ? (
                            <span>{formik.errors.fileOne}</span>
                          ) : null}
                        </Input>
                        <Input>
                          <input
                            type="text"
                            className="form-control w-86"
                            placeholder="+ Документ 2"
                            name="fileTwo"
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            value={formik.values.fileTwo}
                          />
                          {formik.touched.fileTwo && formik.errors.fileTwo ? (
                            <span>{formik.errors.fileTwo}</span>
                          ) : null}
                        </Input>
                      </div>
                      <Input>
                        <textarea
                          className="form-control"
                          rows={3}
                          placeholder="Коментари  "
                          name="detail"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.detail}
                        />
                        {formik.touched.detail && formik.errors.detail ? (
                          <span>{formik.errors.detail}</span>
                        ) : null}
                      </Input>
                    </div>
                    <Buttons className="d-flex align-items-center justify-content-start">
                      <Button
                        className="modify"
                        variant="variantThree"
                        onClick={() => setValue("equipment")}
                      >
                        <img src="/icons/arrow-back.png" alt="" />
                      </Button>
                      <Button variant="variantOne" type="submit">
                        Аплицирај
                      </Button>
                    </Buttons>
                  </div>
                </form>
              </div>
            ) : null}
            {value === "done" ? (
              <>
                <div className="col-6">
                  <img src="/images/applay-3.png" alt="" />
                </div>
                <div className="col-6  text-center my-4">
                  <h2>
                    Ви благодарам за <br /> <span>вашата апликација!</span>
                  </h2>
                  <p className="mx-5">
                    Ќе добиете повратна информација на вашата е-маил адреса за
                    апликацијата кога ќе се донесе одлука.
                  </p>
                  <Link href="/">
                    <Button variant="variantFour">Врати се на почетна</Button>
                  </Link>
                </div>
              </>
            ) : null}
          </div>
        </Container>
      </Parent>
      <ScrollToTop />
    </>
  );
};

export default ApplyForPcEq;
const Parent = styled.div``;
const Container = styled.div`
  margin-top: 114px;
  padding: ${({ theme }) => theme.spacers.XXL}px 0;
  h2 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: 600;
  }
  h3 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: 700;
  }
  h4 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: 700;
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  select {
    width: 25%;
  }

  .hand {
    margin-bottom: 30px;
  }
  ul {
    margin: 0 0 ${({ theme }) => theme.spacers.XXL}px 0;
    padding: 0;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
  li {
    display: flex;
    align-items: center;
    margin-bottom: 10px;
  }
  li img {
    width: 20px;
    margin-right: 10px;
  }
  form p {
    line-height: 1.33;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
`;
const Buttons = styled.div`
  & :first-child {
    border: none;
  }
  & :nth-child(2) {
    margin-left: 10px;
  }
  .modify {
    padding: 7px 20px;
  }
`;
const Input = styled.div`
  margin-bottom: 21px;

  .w-86 {
    width: 86%;
  }
  textarea.form-control {
    min-height: calc(1.5em + 4.2rem + 2px);
  }
`;
