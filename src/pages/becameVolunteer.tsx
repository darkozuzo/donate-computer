import React, { useState } from "react";
import styled from "styled-components";
import Button from "../commonComponents/Button";
import Link from "next/link";
import { useFormik } from "formik";
import { NextPage } from "next";
import ScrollToTop from "../commonComponents/scrollToTop";

const BecameVolunteer: NextPage = () => {
  const [page, setPage] = useState<1 | 2>(1);
  const formik = useFormik({
    initialValues: {
      name: "",
      city: "",
      email: "",
      tel: "",
      position: "",
      detail: "",
      file: "",
    },
    onSubmit: (values) => {
      console.log("formik", values);
      setPage(2);
    },
    validate: (values) => {
      let errors = {};

      if (!values.name) {
        //@ts-ignore
        errors.name = "Required";
      }
      if (!values.city) {
        //@ts-ignore
        errors.city = "Required";
      }

      if (!values.email) {
        //@ts-ignore
        errors.email = "Required";
      } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
      ) {
        //@ts-ignore
        errors.email = "Invalid email address";
      }

      if (!values.tel) {
        //@ts-ignore
        errors.tel = "Required";
      }

      if (!values.position) {
        //@ts-ignore
        errors.position = "Required";
      }

      if (!values.detail) {
        //@ts-ignore
        errors.detail = "Required";
      }

      if (!values.file) {
        //@ts-ignore
        errors.file = "Required";
      }

      return errors;
    },
  });

  return (
    <>
      <Container className="container">
        <div className="row justify-content-center align-item-center text-center">
          {page === 1 ? (
            <>
              <h2>
                Биди <span>Волонтер!</span>
              </h2>
              <div className="col-8 text-start">
                <form onSubmit={formik.handleSubmit}>
                  <div className="row">
                    <div className="col-6">
                      <Input>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Име и Презиме *"
                          name="name"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.name}
                        />
                        {formik.touched.name && formik.errors.name ? (
                          <span>{formik.errors.name}</span>
                        ) : null}
                      </Input>
                      <Input>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Град *"
                          name="city"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.city}
                        />
                        {formik.touched.city && formik.errors.city ? (
                          <span>{formik.errors.city}</span>
                        ) : null}
                      </Input>
                      <Input>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Маил *"
                          name="email"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.email}
                        />
                        {formik.touched.email && formik.errors.email ? (
                          <span>{formik.errors.email}</span>
                        ) : null}
                      </Input>
                      <Input>
                        <input
                          type="tel"
                          className="form-control"
                          placeholder="Телефонски Број *"
                          name="tel"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.tel}
                        />
                        {formik.touched.tel && formik.errors.tel ? (
                          <span>{formik.errors.tel}</span>
                        ) : null}
                      </Input>
                    </div>
                    <div className="col-6 text-start">
                      <Input>
                        <select
                          className="form-select"
                          name="position"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.position}
                        >
                          <option value="Позиција за која аплицирам">
                            Позиција за која аплицирам
                          </option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                        {formik.touched.position && formik.errors.position ? (
                          <span>{formik.errors.position}</span>
                        ) : null}
                      </Input>
                      <Input>
                        <textarea
                          className="form-control"
                          rows={6}
                          placeholder="Детали за твоето волонтирање, искуство, мотивација"
                          name="detail"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.detail}
                        />
                        {formik.touched.detail && formik.errors.detail ? (
                          <span>{formik.errors.detail}</span>
                        ) : null}
                      </Input>
                      <Input>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="+ Прикачи Документ"
                          name="file"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.file}
                        />
                        {formik.touched.file && formik.errors.file ? (
                          <span>{formik.errors.file}</span>
                        ) : null}
                      </Input>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit. Pariatur, a.
                      </p>
                      <Buttons className="d-flex align-items-center justify-content-start">
                        <Link href="/volunteering">
                          <Button className="modify" variant="variantThree">
                            <img src="/icons/arrow-back.png" alt="" />
                          </Button>
                        </Link>

                        <Button variant="variantOne" type="submit">
                          Аплицирај
                        </Button>
                      </Buttons>
                    </div>
                  </div>
                </form>
              </div>
            </>
          ) : null}
          {page === 2 ? (
            <>
              <div className="col-6">
                <img
                  src="/images/banner-images/becomeVolunteerImg-1.png"
                  alt=""
                />
              </div>
              <ThankYou className="col-6">
                <h2 className="mt-4">
                  Ви благодарам на <span>вашата апликација!</span>
                </h2>
                <p className="px-5">
                  Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ut
                  alias aspernatur ad velit architecto necessitatibus ullam
                  illum.
                </p>
                <div className="thankTouInner">
                  <img src="/vectors/becomeVolunteerSvg-3.svg" alt="" />
                </div>
              </ThankYou>
              <div className="col-12 d-flex justify-content-center align-items-center flex-column">
                <h5>Сподели на социјални мрежи</h5>
                <BannerIcons className="col-4 d-flex  justify-content-around align-items-center">
                  <LinkedIn href={"http://www.linkedIn.com"} target="_blank">
                    <img src="/icons/linked-in.png" alt="" />
                  </LinkedIn>
                  <Facebook href={"http://www.facebook.com"} target="_blank">
                    <img className="p-1" src="/icons/facebook.png" alt="" />
                  </Facebook>
                  <Instagram href={"http://www.instagram.com"} target="_blank">
                    <img src="/icons/instagram.png" alt="" />
                  </Instagram>
                  <Twitter href={"https://www.twitter.com"} target="_blank">
                    <img src="/icons/twitter.png" alt="" />
                  </Twitter>
                </BannerIcons>
                <p>
                  <b>ИЛИ</b>
                </p>
                <Link href="/">
                  <Button variant="variantFour">Врати се на почетна</Button>
                </Link>
              </div>
            </>
          ) : null}
        </div>
      </Container>
      <ScrollToTop />
    </>
  );
};

export default BecameVolunteer;

const Container = styled.div`
  margin-top: 114px;
  padding: ${({ theme }) => theme.spacers.XXL}px 0;
  h2 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: 600;
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  h5 {
    font-size: ${({ theme }) => theme.fonts.font_35}px;
    font-weight: 700;
    margin-bottom: ${({ theme }) => theme.spacers.XL}px;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }

  textarea {
    margin-bottom: 21px;
  }
  select {
    margin-bottom: 20px;
  }
`;
const Buttons = styled.div`
  & :first-child {
    border: none;
  }
  & :nth-child(2) {
    margin-left: 10px;
  }
  .modify {
    padding: 7px 20px;
  }
`;

const BannerIcons = styled.div`
  padding: 0 70px;
  margin-bottom: ${({ theme }) => theme.spacers.XL}px;

  img {
    margin: 0 auto;
  }
`;

const Twitter = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;
const Instagram = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;
const Facebook = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;
const LinkedIn = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;

const ThankYou = styled.div`
  position: relative;
  .thankTouInner {
    position: absolute;
    top: 210px;
    right: 250px;
  }
  .thankTouInner img {
    width: 250px;
  }
`;

const Input = styled.div`
  margin-bottom: 21px;
`;
