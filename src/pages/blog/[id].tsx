import { GetStaticPaths, GetStaticProps, NextPage } from "next";
import Link from "next/link";
import React from "react";
import styled from "styled-components";
import CardVariantOne from "../../commonComponents/CardVariantOne";
import Slider from "react-slick";
import { CardOne } from "../../types";
import ScrollToTop from "../../commonComponents/scrollToTop";
import { useRouter } from "next/router";

export const getStaticPaths: GetStaticPaths = async () => {
  const res = await fetch(`http://localhost:5000/en`);
  const data = await res.json();
  const card: CardOne[] = data.card.variant_one;
  const paths = card.map((post) => {
    return {
      params: {
        id: post.id.toString(),
      },
    };
  });
  return {
    paths,
    fallback: false,
  };
};
//tuka bi trebalo da napravam fetch so dinamicen params od nekoe api koe bi bilo izgradeno
export const getStaticProps: GetStaticProps = async ({ locale }) => {
  const res = await fetch(`http://localhost:5000/${locale}`);
  const data = await res.json();
  return {
    props: {
      card: data.card.variant_one,
    },
  };
};
interface Props {
  card: CardOne[];
}

const BlogPost: NextPage<Props> = ({ card }) => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
  };
  const router = useRouter();
  console.log(router, "zuzo");

  return (
    <>
      <Banner className="container text-center">
        <h1>
          БИДИ ВО ТЕК<span>СО НАС</span>!
        </h1>
        <div className="row">
          <BannerLeft className="col-4">
            <div className="borderLeft">
              <img src="/vectors/blog-banner-left.svg" alt="" />
            </div>
          </BannerLeft>
          <div className="col-4"></div>
          <BannerRight className="col-4">
            <div className="borderRight">
              <img src="/vectors/blog-banner-right.svg" alt="" />
            </div>
          </BannerRight>
        </div>
        <div className="row justify-content-center">
          <BannerIcons className="col-4 d-flex  justify-content-around align-items-center">
            <LinkedIn href={"http://www.linkedIn.com"} target="_blank">
              <img src="/icons/linked-in.png" alt="" />
            </LinkedIn>
            <Facebook href={"http://www.facebook.com"} target="_blank">
              <img className="p-1" src="/icons/facebook.png" alt="" />
            </Facebook>
            <Instagram href={"http://www.instagram.com"} target="_blank">
              <img src="/icons/instagram.png" alt="" />
            </Instagram>
            <Twitter href={"https://www.twitter.com"} target="_blank">
              <img src="/icons/twitter.png" alt="" />
            </Twitter>
          </BannerIcons>
        </div>
      </Banner>
      <BlogSection className="container">
        <RowFirst className="row">
          <div className="col-6">
            <img src="/images/card-images/card-1.jpg" alt="" />
          </div>
          <div className="col-6">
            <h4>
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illo
              officia vel dicta eum asperiores.
            </h4>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique
              facilis nisi aperiam esse officia repellat architecto dolorem
              deleniti saepe dignissimos porro molestias vitae magnam laudantium
              officiis ullam corporis, ex eligendi. Lorem ipsum dolor sit amet
              consectetur adipisicing elit. Quibusdam similique voluptas
              corrupti dignissimos fugit architecto reprehenderit minima at
              cupiditate atque facilis quo nemo esse id repudiandae, asperiores
              quaerat voluptatibus sapiente. Lorem ipsum dolor sit amet
              consectetur adipisicing elit. Illo harum, eaque eius quam
              perferendis dolore, dolorem dicta ducimus cumque dignissimos
              delectus aspernatur ex atque pariatur fugiat rem nobis corrupti
              hic. Lorem ipsum dolor sit amet consectetur adipisicing elit. Id
              obcaecati eum incidunt omnis odio aut placeat facilis dolore
              nihil, hic esse facere sequi, maiores quod quae repudiandae.
              Molestiae, inventore id! Lorem, ipsum dolor sit amet consectetur
              adipisicing elit. Odit, voluptatibus
            </p>
          </div>
        </RowFirst>
        <RowSecond className="row">
          <div className="col-6">
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique
              facilis nisi aperiam esse officia repellat architecto dolorem
              deleniti saepe dignissimos porro molestias vitae magnam laudantium
              officiis ullam corporis, ex eligendi. Lorem ipsum dolor sit amet
              consectetur adipisicing elit.<br></br> <br></br> Quibusdam
              similique voluptas corrupti dignissimos fugit architecto
              reprehenderit minima at cupiditate atque facilis quo nemo esse id
              repudiandae, asperiores quaerat voluptatibus sapiente. Lorem ipsum
              dolor sit amet consectetur adipisicing elit. <br></br> <br></br>{" "}
              Illo harum, eaque eius quam perferendis dolore, dolorem dicta
              ducimus cumque dignissimos delectus aspernatur ex atque pariatur
              fugiat rem nobis corrupti hic. Lorem ipsum dolor sit amet
              consectetur adipisicing elit. Id obcaecati eum incidunt omnis odio
              aut placeat facilis dolore nihil, hic esse facere sequi, maiores
              quod quae repudiandae. Molestiae, inventore id! Lorem, ipsum dolor
              sit amet consectetur adipisicing elit. Odit, voluptatibus
            </p>
          </div>
          <div className="col-6">
            <img src="/images/card-images/card-1.jpg" alt="" />
          </div>
        </RowSecond>
        <div className="row">
          <div className="col">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iure
            dolore enim sed numquam corporis nemo voluptas est commodi debitis?
            Autem, quo. Sed laboriosam odit laudantium eligendi autem rerum
            blanditiis maxime? Sed laboriosam odit laudantium eligendi autem
            rerum blanditiis maxime? Lorem, ipsum dolor sit amet consectetur
            adipisicing elit. Iure dolore enim sed numquam corporis nemo
            voluptas est commodi debitis? Autem, quo. Sed laboriosam odit
            laudantium eligendi autem rerum blanditiis maxime? Sed laboriosam
            odit laudantium eligendi autem rerum blanditiis maxime?
          </div>
        </div>
      </BlogSection>
      <ReadNext className="container position-relative">
        <Title className="d-flex justify-content-between">
          <h2>
            Ако <span>ти се допадна</span>, читај следно
          </h2>
          <div className="bgImage"></div>
        </Title>
        <div className="row justify-content-center">
          <div className="col-10">
            <Slide {...settings}>
              {card?.map((c) => {
                return <CardVariantOne key={c.id} {...c} />;
              })}
            </Slide>
          </div>
        </div>
        <Svg>
          <img src="/vectors/share-story-underTitle.svg" alt="" />
        </Svg>
      </ReadNext>
      <ScrollToTop />
    </>
  );
};

export default BlogPost;

const Banner = styled.div`
  margin-top: 114px;
  padding: ${({ theme }) => theme.spacers.XXXL}px 0;
  height: 60vh;

  h1 {
    font-size: ${({ theme }) => theme.fonts.font_72}px;
    font-weight: 800;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
`;

const BannerIcons = styled.div`
  margin: -60px;
  padding: 0 70px;
  img {
    margin: 0 auto;
  }
`;
const BannerLeft = styled.div`
  .borderLeft {
    border-bottom: 10px dotted red;
  }
  img {
    margin-bottom: 10px;
  }
`;
const BannerRight = styled.div`
  .borderRight {
    border-bottom: 10px dotted red;
  }
  img {
    margin-bottom: 10px;
  }
`;

const Twitter = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;
const Instagram = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;
const Facebook = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;
const LinkedIn = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;

const BlogSection = styled.div`
  padding: ${({ theme }) => theme.spacers.XXXL}px 0;
  p {
    margin-bottom: 0px;
  }
`;

const RowFirst = styled.div`
  margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
`;
const RowSecond = styled.div`
  margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  border-left: 10px dotted red;
`;

const ReadNext = styled.div`
  padding: ${({ theme }) => theme.spacers.XXL}px 0;
`;
const Title = styled.div`
  .bgImage {
    background-image: url("/images/readNext.png");
    background-position: center;
    background-repeat: no-repeat;
    background-size: contain;
    height: 190px;
    width: 700px;
    margin-left: -300px;
    margin-top: -30px;
  }
  h2 {
    font-size: ${({ theme }) => theme.fonts.font_32}px;
    font-weight: 600;
    z-index: 99;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
`;
const Svg = styled.div`
  position: absolute;
  top: 70px;
  left: 400px;
`;

const Slide = styled(Slider)`
  .slick-next:before {
    content: url("/icons/right-icon.svg") !important;
  }
  .slick-prev:before {
    content: url("/icons/left-icon.svg") !important;
  }
  .slick-prev {
    left: -90px;
  }
  .slick-next {
    right: -40px;
  }
  .slick-dots {
    display: none !important;
  }
`;
