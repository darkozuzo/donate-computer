import React, { useState } from "react";
import styled from "styled-components";
import Button from "../commonComponents/Button";
import { useFormik } from "formik";
import Link from "next/link";
import ScrollToTop from "../commonComponents/scrollToTop";

const Donate = () => {
  const [value, setValue] = useState<string>("");
  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setValue(event.target.value);
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      lastName: "",
      companyName: "",
      city: "",
      email: "",
      tel: "",
      donate: "",
      numberOfDevices: "",
      ageOfDevices: "",
      operatingSystem: "",
      RAM: "",
      fileOne: "",
      fileTwo: "",
      detail: "",
      hearing: "",
    },
    onSubmit: (values) => {
      console.log("formik", values);
      setValue("done");
    },
    validate: (values) => {
      let errors = {};

      if (!values.name) {
        //@ts-ignore
        errors.name = "Required";
      }
      if (!values.lastName) {
        //@ts-ignore
        errors.lastName = "Required";
      }
      if (!values.companyName) {
        //@ts-ignore
        errors.companyName = "Required";
      }
      if (!values.city) {
        //@ts-ignore
        errors.city = "Required";
      }

      if (!values.email) {
        //@ts-ignore
        errors.email = "Required";
      } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
      ) {
        //@ts-ignore
        errors.email = "Invalid email address";
      }

      if (!values.tel) {
        //@ts-ignore
        errors.tel = "Required";
      }
      if (!values.donate) {
        //@ts-ignore
        errors.donate = "Required";
      }
      if (!values.numberOfDevices) {
        //@ts-ignore
        errors.numberOfDevices = "Required";
      }
      if (!values.ageOfDevices) {
        //@ts-ignore
        errors.ageOfDevices = "Required";
      }
      if (!values.operatingSystem) {
        //@ts-ignore
        errors.operatingSystem = "Required";
      }
      if (!values.RAM) {
        //@ts-ignore
        errors.RAM = "Required";
      }

      if (!values.fileOne) {
        //@ts-ignore
        errors.fileOne = "Required";
      }
      if (!values.fileTwo) {
        //@ts-ignore
        errors.fileTwo = "Required";
      }
      if (!values.detail) {
        //@ts-ignore
        errors.detail = "Required";
      }
      if (!values.hearing) {
        //@ts-ignore
        errors.hearing = "Required";
      }

      return errors;
    },
  });
  return (
    <>
      <Container className="container">
        <div className="row">
          {value === "" ? (
            <>
              <div className="col-6 text-end align-self-center">
                <h3>Сакам да донирам...</h3>
                <select value={value} onChange={handleChange} className="w-50">
                  <option defaultValue={"choose"}>Одбери </option>
                  <option value="computer">
                    Компјутер <img src="/icons/correct-icon.png" alt="" />
                  </option>
                </select>
              </div>
              <div className="col-6 d-flex flex-column align-items-center">
                <div className="hand">
                  <img src="/images/applay-1.png" alt="" />
                </div>
                <div>
                  <img src="/images/applay-4.png" alt="" />
                </div>
              </div>
            </>
          ) : null}
          {value === "computer" ? (
            <>
              <BgColor className="col-6">
                <div className="bgColorInner text-center">
                  <h4>Прифаќам...</h4>
                  <p className="mb-">
                    Уредите кои ги донирате треба да ги
                    <br /> исполнуваат условите за минимална
                    <br /> прифатлива конфигурација:
                  </p>
                  <ul>
                    <li>
                      <img src="/icons/correct-icon.png" alt="" />
                      <b>Минимален ДуалКор процесор</b>
                    </li>
                    <li>
                      <img src="/icons/correct-icon.png" alt="" />
                      <b>Минимални 2 GB РАМ меморија</b>
                    </li>
                    <li>
                      <img src="/icons/correct-icon.png" alt="" />
                      <b>Минимални 160 GB хард диск</b>
                    </li>
                  </ul>
                  <Buttons className="d-flex align-items-center justify-content-center">
                    <Button
                      className="modify"
                      variant="variantThree"
                      onClick={() => setValue("")}
                    >
                      <img src="/icons/arrow-back.png" alt="" />
                    </Button>
                    <Button
                      variant="variantOne"
                      onClick={() => setValue("computerForm")}
                    >
                      Продолжи
                    </Button>
                  </Buttons>
                </div>
              </BgColor>
              <BgColor className="col-6">
                <div className="bgColorInner text-center">
                  <h4>Не прифаќам...</h4>
                  <p className="mb-">
                    Секој уред кој не ги исполнува
                    <br /> минималните барања и исто така има:
                  </p>
                  <ul className="added-bottom-space">
                    <li>
                      <img src="/icons/correct-icon.png" alt="" />
                      <b>Многу стар оперативен систем</b>
                    </li>
                    <li>
                      <img src="/icons/correct-icon.png" alt="" />
                      <b>Нефункционален хардвер</b>
                    </li>
                    <li>
                      <img src="/icons/correct-icon.png" alt="" />
                      <b>Многу оштетен уред</b>
                    </li>
                  </ul>
                </div>
              </BgColor>
            </>
          ) : null}
          {value === "computerForm" ? (
            <div className="col-12">
              <h3>Ајде да донираме!</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur <br />
                adipisicing elit. Repellat, aut.
              </p>
              <form onSubmit={formik.handleSubmit}>
                <div className="row">
                  <div className="col-4">
                    <Input>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Име *"
                        name="name"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.name}
                      />
                      {formik.touched.name && formik.errors.name ? (
                        <span>{formik.errors.name}</span>
                      ) : null}
                    </Input>
                    <Input>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Презиме *"
                        name="lastName"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.lastName}
                      />
                      {formik.touched.lastName && formik.errors.lastName ? (
                        <span>{formik.errors.lastName}</span>
                      ) : null}
                    </Input>
                    <Input>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Име на компанија *"
                        name="companyName"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.companyName}
                      />
                      {formik.touched.companyName &&
                      formik.errors.companyName ? (
                        <span>{formik.errors.companyName}</span>
                      ) : null}
                    </Input>
                    <Input>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Маил *"
                        name="email"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.email}
                      />
                      {formik.touched.email && formik.errors.email ? (
                        <span>{formik.errors.email}</span>
                      ) : null}
                    </Input>
                    <Input>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Град *"
                        name="city"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.city}
                      />
                      {formik.touched.city && formik.errors.city ? (
                        <span>{formik.errors.city}</span>
                      ) : null}
                    </Input>
                    <Input>
                      <input
                        type="tel"
                        className="form-control"
                        placeholder="Телефонски Број *"
                        name="tel"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.tel}
                      />
                      {formik.touched.tel && formik.errors.tel ? (
                        <span>{formik.errors.tel}</span>
                      ) : null}
                    </Input>
                  </div>
                  <div className="col-4">
                    <Input>
                      <select
                        className="form-select w-100"
                        name="donate"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.donate}
                      >
                        <option value="Сакам да донирам">
                          Сакам да донирам
                        </option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                      {formik.touched.donate && formik.errors.donate ? (
                        <span>{formik.errors.donate}</span>
                      ) : null}
                    </Input>
                    <Input>
                      <select
                        className="form-select w-100"
                        name="numberOfDevices"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.numberOfDevices}
                      >
                        <option value="Број на уреди">Број на уреди</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                      {formik.touched.numberOfDevices &&
                      formik.errors.numberOfDevices ? (
                        <span>{formik.errors.numberOfDevices}</span>
                      ) : null}
                    </Input>
                    <Input>
                      <select
                        className="form-select w-100"
                        name="ageOfDevices"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.ageOfDevices}
                      >
                        <option value="Старост на уредот">
                          Старост на уредот
                        </option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                      {formik.touched.ageOfDevices &&
                      formik.errors.ageOfDevices ? (
                        <span>{formik.errors.ageOfDevices}</span>
                      ) : null}
                    </Input>
                    <Input>
                      <select
                        className="form-select w-100"
                        name="operatingSystem"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.operatingSystem}
                      >
                        <option value="Оперативен систем">
                          Оперативен систем
                        </option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                      {formik.touched.operatingSystem &&
                      formik.errors.operatingSystem ? (
                        <span>{formik.errors.operatingSystem}</span>
                      ) : null}
                    </Input>
                    <Input>
                      <select
                        className="form-select w-100"
                        name="RAM"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.RAM}
                      >
                        <option value="RAM">RAM</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                      {formik.touched.RAM && formik.errors.RAM ? (
                        <span>{formik.errors.RAM}</span>
                      ) : null}
                    </Input>
                  </div>
                  <div className="col-4">
                    <div className="d-flex">
                      <Input>
                        <input
                          type="text"
                          className="form-control w-70"
                          placeholder="+ Документ 1+"
                          name="fileOne"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.fileOne}
                        />
                        {formik.touched.fileOne && formik.errors.fileOne ? (
                          <span>{formik.errors.fileOne}</span>
                        ) : null}
                      </Input>
                      <Input>
                        <input
                          type="text"
                          className="form-control w-70"
                          placeholder="+ Документ 2"
                          name="fileTwo"
                          onBlur={formik.handleBlur}
                          onChange={formik.handleChange}
                          value={formik.values.fileTwo}
                        />
                        {formik.touched.fileTwo && formik.errors.fileTwo ? (
                          <span>{formik.errors.fileTwo}</span>
                        ) : null}
                      </Input>
                    </div>
                    <p className="paragraph">
                      Можете да прикачувате датотеки на вашата компанија, како и
                      фотографии/видеа од вашата донација на опрема.
                    </p>
                    <Input>
                      <textarea
                        className="form-control"
                        rows={3}
                        placeholder="Коментари  "
                        name="detail"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.detail}
                      />
                      {formik.touched.detail && formik.errors.detail ? (
                        <span>{formik.errors.detail}</span>
                      ) : null}
                    </Input>
                    <Input>
                      <textarea
                        className="form-control"
                        rows={3}
                        placeholder="Како слушна за нас  "
                        name="hearing"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.hearing}
                      />
                      {formik.touched.hearing && formik.errors.hearing ? (
                        <span>{formik.errors.hearing}</span>
                      ) : null}
                    </Input>
                  </div>
                  <Buttons className="d-flex align-items-center justify-content-end">
                    <Button
                      className="modify"
                      variant="variantThree"
                      onClick={() => setValue("computer")}
                    >
                      <img src="/icons/arrow-back.png" alt="" />
                    </Button>
                    <Button type="submit" variant="variantOne">
                      Аплицирај
                    </Button>
                  </Buttons>
                </div>
              </form>
            </div>
          ) : null}
          {value === "done" ? (
            <>
              <div className="col-6">
                <img src="/images/applay-5.png" alt="" />
              </div>
              <ThankYou className="col-6 text-center">
                <h2 className="mt-4">
                  Ви благодарам на <span>вашата апликација!</span>
                </h2>
                <p className="px-5">
                  Ако сакаш да ја рашириш веста за нашите и твоите дела, сподели
                  со твоите пријатели.
                </p>
                <div className="thankTouInner">
                  <img src="/vectors/becomeVolunteerSvg-3.svg" alt="" />
                </div>
              </ThankYou>
              <div className="col-12 d-flex justify-content-center align-items-center flex-column">
                <h5>Сподели на социјални мрежи</h5>
                <BannerIcons className="col-4 d-flex  justify-content-around align-items-center">
                  <LinkedIn href={"http://www.linkedIn.com"} target="_blank">
                    <img src="/icons/linked-in.png" alt="" />
                  </LinkedIn>
                  <Facebook href={"http://www.facebook.com"} target="_blank">
                    <img className="p-1" src="/icons/facebook.png" alt="" />
                  </Facebook>
                  <Instagram href={"http://www.instagram.com"} target="_blank">
                    <img src="/icons/instagram.png" alt="" />
                  </Instagram>
                  <Twitter href={"https://www.twitter.com"} target="_blank">
                    <img src="/icons/twitter.png" alt="" />
                  </Twitter>
                </BannerIcons>
                <Link href="/">
                  <Button variant="variantFour">Врати се на почетна</Button>
                </Link>
              </div>
            </>
          ) : null}
        </div>
      </Container>
      <ScrollToTop />
    </>
  );
};

export default Donate;
const Container = styled.div`
  margin-top: 114px;
  padding: ${({ theme }) => theme.spacers.XL}px 0;
  h2 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: 600;
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  h3 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: 700;
  }
  h4 {
    font-size: ${({ theme }) => theme.fonts.font_50}px;
    font-weight: 700;
    margin-bottom: ${({ theme }) => theme.spacers.XXL}px;
  }
  h5 {
    font-size: ${({ theme }) => theme.fonts.font_35}px;
    font-weight: 700;
    margin-bottom: ${({ theme }) => theme.spacers.XL}px;
  }
  ul {
    margin: 0 0 ${({ theme }) => theme.spacers.XXL}px 140px !important;
    margin: 0;
    padding: 0;
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
  li {
    display: flex;
    align-items: center;
    margin-bottom: 10px;
  }
  li img {
    width: 20px;
    margin-right: 10px;
  }
  .paragraph {
    font-size: 14px;
  }
  span {
    color: ${({ theme }) => theme.colors.customRed};
  }
`;
const Buttons = styled.div`
  & :first-child {
    border: none;
  }
  & :nth-child(2) {
    margin-left: 10px;
  }
  .modify {
    padding: 7px 20px;
  }
`;
const Input = styled.div`
  margin-bottom: 21px;

  .w-70 {
    width: 70% !important;
    margin-right: 5px;
  }
  textarea.form-control {
    min-height: calc(1.5em + 4.2rem + 2px);
  }
`;
const BgColor = styled.div`
  .bgColorInner {
    background-color: ${({ theme }) => theme.colors.custom1};
    border-radius: 4%;
    padding: 20px 0 20px 0;
  }
  .added-bottom-space {
    padding-bottom: 72px;
  }
`;

const BannerIcons = styled.div`
  padding: 0 70px;
  margin-bottom: ${({ theme }) => theme.spacers.XXL}px;

  img {
    margin: 0 auto;
  }
`;
const Twitter = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;
const Instagram = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;
const Facebook = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;
const LinkedIn = styled(Link)`
  width: 45px;
  height: 45px;
  border: 2px solid ${({ theme }) => theme.colors.customRed};
  border-radius: 50%;
  display: flex;
  align-items: center;
  padding: 8px;
  cursor: pointer;
`;

const ThankYou = styled.div`
  position: relative;
  .thankTouInner {
    position: absolute;
    top: 210px;
    right: 250px;
  }
  .thankTouInner img {
    width: 250px;
  }
`;
