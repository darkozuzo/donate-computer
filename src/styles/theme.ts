
const colors = {
    red: 'red',
    white: "white",
    black: 'black',
    yellow: 'yellow',
    customRed: '#D73F3B',
    customDarkRed: 'darkRed',
    custom1: "#8AA6BF",
    custom2: "#F9F7F7",
    custom3: "#F7D9D8",
    custom4: "#6A6A6A",
}
const spacers = {
    S: 2,
    M: 4,
    L: 8,
    XL: 16,
    XXL: 32,
    XXXL: 64,

}

const fonts = {
    font_18: 18,
    font_20: 20,
    font_24: 24,
    font_32: 32,
    font_50: 50,
    font_72: 72,
    font_100: 100,
}

const weights = {
    weight_400: 400,
    weight_500: 500,
    weight_600: 600,
    weight_700: 700,
    weight_800: 800,
}
export const theme = {
    colors,
    spacers,
    fonts,
    weights
}