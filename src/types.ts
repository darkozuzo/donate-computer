export type ImageType = {
    id: number;
    img: string;
};
export type CardOne = {
    id: number
    img: string
    title: string
    preTitle: string
    text: string
    date: string
}
export type CardTwo = {
    id: number
    img: string
    title: string
    preTitle: string
    text: string
}

export type TestimonialsType = {
    id: number;
    name: string;
    age: number;
    text: string;
    img: string
}
export type Partners = {
    company: string

}